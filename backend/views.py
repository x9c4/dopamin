from django.contrib.auth import get_user_model
from django.contrib.auth import logout as auth_logout
from django.shortcuts import get_object_or_404
from django_filters import rest_framework as filters
from drf_spectacular.utils import extend_schema, inline_serializer
from rest_framework import generics, mixins, permissions, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Adjudicator, Couple, Heat, Mark, Round, Tournament
from .serializers import (
    AdjudicatorSerializer,
    CoupleSerializer,
    HeatSerializer,
    LoginSerializer,
    MarkSerializer,
    RoundQualifySerializer,
    RoundResultSerializer,
    RoundSerializer,
    RoundShuffleSerializer,
    TournamentSerializer,
    UserSerializer,
)


class StateMixin:
    @extend_schema(
        request=inline_serializer(name="EmptySerializer", fields={}),
    )
    @action(detail=True, methods=["post"])
    def reset(self, request, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        instance.reset()
        return Response(serializer.data)

    @extend_schema(
        request=inline_serializer(name="EmptySerializer", fields={}),
    )
    @action(detail=True, methods=["post"])
    def start(self, request, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        serializer.validate_start()
        instance.start()
        return Response(serializer.data)

    @extend_schema(
        request=inline_serializer(name="EmptySerializer", fields={}),
    )
    @action(detail=True, methods=["post"])
    def finish(self, request, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        serializer.validate_finish()
        instance.finish()
        return Response(serializer.data)


class NestedTournamentViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        return super().get_queryset().filter(tournament=self.kwargs["tournament_pk"])

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["tournament"] = get_object_or_404(
            Tournament.objects, pk=self.kwargs["tournament_pk"]
        )
        return context


class LoginView(generics.CreateAPIView):
    serializer_class = LoginSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request):
        return Response(self.get_serializer(request.user).data)

    def delete(self, request):
        auth_logout(request)
        return Response(self.get_serializer(request.user).data)


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAdminUser]

    serializer_class = UserSerializer
    queryset = get_user_model().objects.order_by("pk")
    filterset_fields = {"username": ["exact", "iexact", "contains", "icontains"]}


class TournamentViewSet(viewsets.ModelViewSet, StateMixin):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = TournamentSerializer
    queryset = Tournament.objects.order_by("pk")
    filterset_fields = {
        "name": ["exact", "iexact", "contains", "icontains"],
    }


class RoundViewSet(NestedTournamentViewSet, StateMixin):
    serializer_class = RoundSerializer
    queryset = Round.objects.order_by("id")
    filterset_fields = {
        "round_type": ["exact"],
        "slug": ["exact"],
    }

    # TODO Disqualify

    @extend_schema(
        request=RoundQualifySerializer,
        responses={200: inline_serializer(name="EmptySerializer", fields={})},
    )
    @action(detail=True, methods=["post"])
    def qualify(self, request, **kwargs):
        instance = self.get_object()
        serializer = RoundQualifySerializer(
            data=request.data, context={"round": instance, "tournament": instance.tournament}
        )
        serializer.is_valid(raise_exception=True)
        instance.couples.add(*serializer.validated_data["couples"])
        return Response()

    @extend_schema(
        request=RoundShuffleSerializer,
        responses={200: RoundShuffleSerializer},
    )
    @action(detail=True, methods=["get", "post"])
    def shuffle(self, request, **kwargs):
        instance = self.get_object()
        context = {"round": instance, "tournament": instance.tournament}
        if request.method == "POST":
            serializer = RoundShuffleSerializer(data=request.data, context=context)
            serializer.is_valid(raise_exception=True)
            instance.shuffle(
                heats=serializer.validated_data["heats"],
                heat_couples=serializer.validated_data["heat_couples"],
                force=serializer.validated_data["force"],
            )
        serializer = RoundShuffleSerializer(instance=instance, context=context)
        return Response(serializer.data)

    @extend_schema(
        responses={200: RoundResultSerializer},
    )
    @action(detail=True, methods=["get"])
    def results(self, request, **kwargs):
        instance = self.get_object()
        context = {"round": instance, "tournament": instance.tournament}
        serializer = RoundResultSerializer(instance=instance, context=context)
        return Response(serializer.data)


class HeatFilter(filters.FilterSet):
    tournament = filters.ModelChoiceFilter(
        queryset=Tournament.objects.all(), field_name="round__tournament"
    )

    class Meta:
        model = Heat
        fields = {
            "round": ["exact"],
            "number": ["exact"],
            "dance": ["exact"],
        }


class HeatViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = HeatSerializer
    queryset = Heat.objects.order_by("id")
    filterset_class = HeatFilter


class AdjudicatorViewSet(NestedTournamentViewSet):
    queryset = Adjudicator.objects.order_by("id")
    serializer_class = AdjudicatorSerializer
    filterset_fields = {
        "slug": ["exact"],
        "name": ["exact", "iexact", "contains", "icontains"],
    }

    def get_queryset(self):
        return super().get_queryset().select_related("user")


class CoupleViewSet(NestedTournamentViewSet):
    queryset = Couple.objects.order_by("id")
    serializer_class = CoupleSerializer
    filterset_fields = {
        "number": ["exact"],
    }


class MarkFilter(filters.FilterSet):
    tournament = filters.ModelChoiceFilter(
        queryset=Tournament.objects.all(), field_name="adjudicator__tournament"
    )
    round = filters.ModelChoiceFilter(queryset=Round.objects.all(), field_name="heat__round")

    class Meta:
        model = Mark
        fields = {
            "adjudicator": ["exact"],
            "couple": ["exact"],
            "heat": ["exact"],
        }


class MarkViewSet(mixins.UpdateModelMixin, viewsets.ReadOnlyModelViewSet):
    serializer_class = MarkSerializer
    queryset = Mark.objects.all()
    filterset_class = MarkFilter
