from drf_spectacular.utils import extend_schema
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Adjudicator, Round
from .v0_serializers import V0AdjudicatorSerializer, V0RoundResultSerializer, V0RoundSerializer
from .views import AdjudicatorViewSet, RoundViewSet


class V0AdjudicatorViewSet(AdjudicatorViewSet):
    serializer_class = V0AdjudicatorSerializer
    queryset = Adjudicator.objects.filter(tournament_id=1)
    lookup_field = "slug"
    filterset_fields = {
        "slug": ["exact"],
        "name": ["exact", "iexact", "contains", "icontains"],
    }


class V0RoundViewSet(RoundViewSet):
    serializer_class = V0RoundSerializer
    queryset = Round.objects.filter(tournament_id=1)
    lookup_field = "slug"
    filterset_fields = {
        "round_type": ["exact"],
        "slug": ["exact"],
    }

    @extend_schema(
        responses={200: V0RoundResultSerializer},
    )
    @action(detail=True, methods=["get"])
    def results(self, request, **kwargs):
        instance = self.get_object()
        context = {"round": instance, "tournament": instance.tournament}
        serializer = V0RoundResultSerializer(instance=instance, context=context)
        return Response(serializer.data)
