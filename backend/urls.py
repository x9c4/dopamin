from django.urls import include, path
from rest_framework_nested import routers

from . import v0_views, views

router = routers.DefaultRouter()
router.register(r"user", views.UserViewSet, "user")
router.register(r"tournament", views.TournamentViewSet, "tournament")
router.register(r"heat", views.HeatViewSet, "heat")
router.register(r"mark", views.MarkViewSet, "mark")

tournament_router = routers.NestedDefaultRouter(router, r"tournament", lookup="tournament")
tournament_router.register(r"adjudicator", views.AdjudicatorViewSet, basename="adjudicator")
tournament_router.register(r"couple", views.CoupleViewSet, basename="couple")
tournament_router.register(r"round", views.RoundViewSet, basename="round")

urlpatterns = [
    path(r"login/", views.LoginView.as_view()),
    path("", include(router.urls)),
    path("", include(tournament_router.urls)),
]

v0_router = routers.DefaultRouter()
v0_router.register(r"adjudicators", v0_views.V0AdjudicatorViewSet, "v0_adjudicator")
v0_router.register(r"rounds", v0_views.V0RoundViewSet, "v0_round")
