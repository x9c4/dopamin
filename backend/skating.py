# https://tso.turnierprotokoll.de/tso_anh2.htm

from collections import defaultdict


class Placement:
    def __init__(self, from_place, couples):
        assert couples
        self.from_place = from_place
        self.couples = couples

    def split(self, split_couples):
        rest_couples = [couple for couple in self.couples if couple not in split_couples]
        if rest_couples:
            return Placement(self.from_place, split_couples), Placement(
                self.from_place + len(split_couples), rest_couples
            )
        else:
            return self, None

    @property
    def to_place(self):
        return self.from_place + len(self.couples) - 1

    @property
    def value(self):
        return (self.from_place + self.to_place) / 2

    @property
    def single(self):
        return len(self.couples) == 1

    def __repr__(self):
        if self.single:
            return f"{self.from_place}"
        else:
            return f"{self.from_place}-{self.to_place}"


class SkatingReport:
    def __init__(self, dances, couples, adjudicators):
        self._dances = dances
        self._couples = couples
        self._adjudicators = adjudicators

        self._n_dances = len(self._dances)
        self._n_couples = len(self._couples)
        self._n_adj = len(self._adjudicators)
        self._results = {
            couple: {
                "marks": [[None] * self._n_adj for i in range(self._n_dances)],
                "majorities": [[None] * self._n_couples for i in range(self._n_dances)],
                "placements": [None] * self._n_dances,
                "sum": None,
                "rule11": [None] * self._n_couples,
                "placement": None,
            }
            for couple in couples
        }

    def mark(self, dance, adj, marks):
        assert sorted(marks) == list(range(1, self._n_couples + 1))
        dance_id = self._dances.index(dance)
        adj_id = self._adjudicators.index(adj)
        for couple, mark in zip(self._couples, marks):
            self._results[couple]["marks"][dance_id][adj_id] = mark

    def evaluate(self):
        for dance_id in range(self._n_dances):
            for placement in self.skate_dance(dance_id):
                for couple in placement.couples:
                    self._results[couple]["placements"][dance_id] = placement

        for placement in self.skate():
            for couple in placement.couples:
                self._results[couple]["placement"] = placement

    def count_sum(self, dance_id, place, couple):
        if dance_id is None:
            marks = sum(self._results[couple]["marks"], [])
            data = self._results[couple]["rule11"]
        else:
            marks = self._results[couple]["marks"][dance_id]
            data = self._results[couple]["majorities"][dance_id]
        result = data[place - 1]
        if result is None:
            lte_marks = [mark for mark in marks if mark <= place]
            result = len(lte_marks), sum(lte_marks)
            if result[0]:
                data[place - 1] = result
        return result

    def skate_dance(self, dance_id):
        placement = Placement(1, self._couples)
        place = 0
        while placement is not None:
            split_couples = []
            while not split_couples:
                place += 1
                for couple in placement.couples:
                    count_marks, sum_marks = self.count_sum(dance_id, place, couple)
                    if count_marks * 2 > self._n_adj:
                        split_couples.append(couple)
            high_placement, placement = placement.split(split_couples)
            yield from self.big_majority(dance_id, place, high_placement)

    def big_majority(self, dance_id, place, placement):
        if placement.single:
            yield placement
        elif place > self._n_couples:
            # Rule 7c
            yield placement
        else:
            # Rule 6
            count_couples = defaultdict(list)
            for couple in placement.couples:
                count_couples[self.count_sum(dance_id, place, couple)[0]].append(couple)
            keys = sorted(count_couples.keys(), reverse=True)
            for key in keys:
                high_placement, placement = placement.split(count_couples[key])
                yield from self.low_sum(dance_id, place, high_placement)
            assert placement is None

    def low_sum(self, dance_id, place, placement):
        if placement.single:
            yield placement
        else:
            # Rule 7a
            sum_couples = defaultdict(list)
            for couple in placement.couples:
                sum_couples[self.count_sum(dance_id, place, couple)[1]].append(couple)
            keys = sorted(sum_couples.keys())
            for key in keys:
                high_placement, placement = placement.split(sum_couples[key])
                # This is basically rule 7b
                yield from self.big_majority(dance_id, place + 1, high_placement)
            assert placement is None

    def skate(self):
        placement = Placement(1, self._couples)
        for couple in placement.couples:
            self._results[couple]["sum"] = sum(
                (i.value for i in self._results[couple]["placements"])
            )
        if placement.single:
            yield placement
        else:
            sum_couples = defaultdict(list)
            for couple in placement.couples:
                sum_couples[self._results[couple]["sum"]].append(couple)
            yield placement
            keys = sorted(sum_couples.keys())
            for key in keys:
                high_placement, placement = placement.split(sum_couples[key])
                yield from self.rule10(high_placement)
            assert placement is None

    def rule10(self, placement):
        if placement.single:
            yield placement
        else:
            best_couples = []
            count_won = 0
            sum_won = 0
            for couple in placement.couples:
                relevant_placements = [
                    i
                    for i in self._results[couple]["placements"]
                    if i.value <= placement.from_place
                ]
                new_count_won = len(relevant_placements)
                new_sum_won = sum((i.value for i in relevant_placements))
                if new_count_won > count_won:
                    best_couples = [couple]
                    count_won = new_count_won
                    sum_won = new_sum_won
                elif new_count_won == count_won:
                    if new_sum_won < sum_won:
                        best_couples = [couple]
                        # count_won = new_count_won
                        sum_won = new_sum_won
                    elif new_sum_won == sum_won:
                        best_couples.append(couple)
                        # count_won = new_count_won
                        # sum_won = new_sum_won
            if best_couples:
                high_placement, placement = placement.split(best_couples)
                yield from self.rule11(high_placement)
                if placement is not None:
                    yield from self.rule10(placement)
            else:
                yield from self.rule11(placement)

    def rule11(self, placement):
        if placement.single:
            yield placement
        elif placement.from_place == self._n_couples:
            yield placement
        else:
            place = placement.from_place
            left_couples = placement.couples
            while place <= self._n_couples:
                best_couples = []
                count_marks = 0
                sum_marks = 0
                for couple in left_couples:
                    new_count_marks, new_sum_marks = self.count_sum(None, place, couple)
                    if new_count_marks * 2 > self._n_adj * self._n_dances:
                        if new_count_marks > count_marks:
                            best_couples = [couple]
                            count_marks = new_count_marks
                            sum_marks = new_sum_marks
                        elif new_count_marks == count_marks:
                            if new_sum_marks < sum_marks:
                                best_couples = [couple]
                                # count_marks = new_count_marks
                                sum_marks = new_sum_marks
                            elif new_sum_marks == sum_marks:
                                best_couples.append(couple)
                                # count_marks = new_count_marks
                                # sum_marks = new_sum_marks
                if len(best_couples) == 1:
                    break
                place += 1
                if best_couples:
                    left_couples = best_couples
            high_placement, placement = placement.split(best_couples)
            yield high_placement
            if placement is not None:
                yield from self.rule10(placement)

    def dance_report(self, dance):
        dance_id = self._dances.index(dance)
        return {
            couple: {
                "marks": self._results[couple]["marks"][dance_id],
                "majorities": self._results[couple]["majorities"][dance_id],
                "placement": str(self._results[couple]["placements"][dance_id]),
            }
            for couple in self._couples
        }

    def final_report(self):
        return {
            couple: {
                "placements": [i.value for i in self._results[couple]["placements"]],
                "sum": self._results[couple]["sum"],
                "rule11": self._results[couple]["rule11"],
                "placement": str(self._results[couple]["placement"]),
            }
            for couple in self._couples
        }


if __name__ == "__main__":
    import json
    import random

    from tabulate import tabulate

    N_COUPLES = 7
    N_ADJ = 9
    N_DANCES = 3

    dances = [str(i + 1) * 3 for i in range(N_DANCES)]
    couples = [str(i + 1) for i in range(N_COUPLES)]
    adjudicators = [chr(ord("A") + i) for i in range(N_ADJ)]

    # initialize data structure
    report = SkatingReport(dances, couples, adjudicators)

    # generate random marks
    for dance in dances:
        for adj in adjudicators:
            marks = list(range(1, N_COUPLES + 1))
            random.shuffle(marks)
            report.mark(dance, adj, marks)

    # with open("results.json", "w") as fp:
    #     json.dump(report._results, fp)

    # Calculate
    report.evaluate()

    # Show results
    for dance in dances:
        dance_report = report.dance_report(dance)
        headers = [dance] + adjudicators + [f"1-{i+1}" for i in range(N_COUPLES)] + ["place"]
        table = [
            [couple]
            + dance_report[couple]["marks"]
            + dance_report[couple]["majorities"]
            + [dance_report[couple]["placement"]]
            for couple in couples
        ]
        print(tabulate(table, headers, tablefmt="pretty"))

    final_report = report.final_report()
    headers = (
        ["final"]
        + [str(i + 1) * 3 for i in range(N_DANCES)]
        + ["sum"]
        + [f"1-{i+1}" for i in range(N_COUPLES)]
        + ["placement"]
    )
    table = [
        [couple]
        + final_report[couple]["placements"]
        + [final_report[couple]["sum"]]
        + final_report[couple]["rule11"]
        + [final_report[couple]["placement"]]
        for couple in couples
    ]
    print(tabulate(table, headers, tablefmt="pretty"))
