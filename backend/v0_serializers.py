from typing import Dict

from django.db import transaction
from rest_framework import serializers

from .models import Adjudicator, Mark, Tournament
from .serializers import HeatSerializer, RoundResultSerializer, RoundSerializer


class V0AdjudicatorSerializer(serializers.ModelSerializer):
    tournament = serializers.HiddenField(default=lambda: Tournament.objects.get(pk=1))

    class Meta:
        model = Adjudicator
        fields = ("tournament", "slug", "name")


class V0CoupleField(serializers.SlugRelatedField):
    def __init__(self, **kwargs):
        slug_field = kwargs.pop("slug_field", "number")
        super().__init__(slug_field=slug_field, **kwargs)

    def get_queryset(self):
        return Tournament.objects.get(pk=1).couples.all()


class V0HeatSerializer(HeatSerializer):
    couples = V0CoupleField(many=True)

    class Meta(HeatSerializer.Meta):
        fields = ("couples", "dance", "number")


class V0RoundSerializer(RoundSerializer):
    tournament = serializers.HiddenField(default=lambda: Tournament.objects.get(pk=1))
    heats = V0HeatSerializer(many=True)

    def create(self, validated_data):
        heats = validated_data.pop("heats")
        with transaction.atomic():
            instance = super().create(validated_data)
            for heat in heats:
                couples = heat.pop("couples")
                heat_instance = instance.heats.create(**heat)
                heat_instance.couples.set(couples)
                for adjudicator in validated_data["tournament"].adjudicators.all():
                    for couple in couples:
                        Mark.objects.get_or_create(
                            couple=couple,
                            adjudicator=adjudicator,
                            heat=heat_instance,
                            defaults={"value": 0},
                        )

        return instance

    def update(self, instance, validated_data):
        heats = validated_data.pop("heats")
        if heats:
            raise serializers.ValidationError("Updating heats is not implemented")
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            for heat in heats:
                # TODO
                pass
        return instance

    class Meta(RoundSerializer.Meta):
        fields = ("tournament", "state", "slug", "marks_target", "heats")


class V0RoundResultSerializer(RoundResultSerializer):
    def get_marks(self, obj) -> Dict[str, int]:
        tournament = self.context["tournament"]
        round = self.context["round"]
        return {
            adjudicator.slug: {
                dance: {
                    couple.number: Mark.objects.get(
                        heat__in=round.heats.all(),
                        heat__dance=dance,
                        couple=couple,
                        adjudicator=adjudicator,
                    ).value
                    for couple in round.couples.all()
                }
                for dance in round.dances
            }
            for adjudicator in tournament.adjudicators.all()
        }
