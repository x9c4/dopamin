import random
from typing import Dict, List, Union

from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth import login as auth_login
from django.contrib.auth import password_validation
from django.contrib.auth.hashers import make_password
from django.db.models import Sum
from django.urls import reverse
from rest_framework import serializers, validators
from rest_framework.utils.urls import replace_query_param
from rest_framework_nested import serializers as serializers_nested
from rest_framework_nested import relations as relations_nested

from .models import Adjudicator, Couple, Heat, Mark, Round, State, Tournament


class UniqueForTournamentValidator(validators.UniqueValidator):
    requires_context = True

    def __call__(self, value, serializer_field):
        # Determine the underlying model field name. This may not be the
        # same as the serializer field name if `source=<>` is set.
        field_name = serializer_field.source_attrs[-1]
        # Determine the existing instance, if this is an update operation.
        instance = getattr(serializer_field.parent, "instance", None)

        queryset = self.queryset
        # ---
        queryset = queryset.filter(tournament__pk=serializer_field.context["tournament"].pk)
        # ---
        queryset = self.filter_queryset(value, queryset, field_name)
        queryset = self.exclude_current_instance(queryset, instance)
        if validators.qs_exists(queryset):
            raise serializers.ValidationError(self.message, code="unique")


class LinkedIdentityField(serializers_nested.NestedHyperlinkedIdentityField):
    @property
    def context(self):
        res = dict(super().context)
        res["request"] = None
        return res


class LinkedRelatedField(serializers_nested.NestedHyperlinkedRelatedField):
    parent_lookup_kwargs = {}

    @property
    def context(self):
        res = dict(super().context)
        res["request"] = None
        return res


class RoundField(LinkedRelatedField):
    parent_lookup_kwargs = {"tournament_pk": "tournament__pk"}
    def __init__(self, **kwargs):
        view_name = kwargs.pop("view_name", "round-detail")
        super().__init__(view_name=view_name, **kwargs)

    def get_queryset(self):
        return self.context["tournament"].rounds.all()


class CoupleField(LinkedRelatedField):
    parent_lookup_kwargs = {"tournament_pk": "tournament__pk"}

    def __init__(self, scope="tournament", **kwargs):
        view_name = kwargs.pop("view_name", "couple-detail")
        super().__init__(view_name=view_name, **kwargs)
        self.scope = scope

    def get_queryset(self):
        return self.context[self.scope].couples.all()


class LinkedModelSerializer(serializers_nested.NestedHyperlinkedModelSerializer):
    url_field_name = "href"
    parent_lookup_kwargs = {}
    serializer_url_field = LinkedIdentityField
    serializer_related_field = LinkedRelatedField

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # TODO Make them known to spectacular
        immutable_fields = getattr(self.Meta, "immutable_fields", None)
        if immutable_fields:
            request = self.context.get("request")
            if request is not None and request.method in ["PUT", "PATCH"]:
                for name in immutable_fields:
                    if name in self.fields:
                        self.fields[name].read_only = True

    def get_default_field_names(self, declared_fields, model_info):
        return [model_info.pk.name] + super().get_default_field_names(declared_fields, model_info)

    def build_nested_field(self, field_name, relation_info, nested_depth):
        """
        Create nested fields for forward and reverse relationships.
        """

        class NestedSerializer(LinkedModelSerializer):
            class Meta:
                model = relation_info.related_model
                depth = nested_depth - 1
                fields = "__all__"

        field_class = serializers.NestedSerializer
        field_kwargs = serializers.get_nested_relation_kwargs(relation_info)

        return field_class, field_kwargs


class StateSerializer(LinkedModelSerializer):
    # state = serializers.CharField(read_only=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["state"].read_only = True

    def validate_start(self):
        if self.instance.state != State.PLANNED:
            raise serializers.ValidationError(f"{self.Meta.model.__name__} is not planned.")

    def validate_finish(self):
        if self.instance.state != State.STARTED:
            raise serializers.ValidationError(f"{self.Meta.model.__name__} is not started.")


class LoginSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    username = serializers.SlugField(required=True)
    password = serializers.CharField(required=True, write_only=True, trim_whitespace=False)

    def validate(self, data):
        data = super().validate(data)
        user = authenticate(
            self.context["request"], username=data["username"], password=data["password"]
        )
        if user is None:
            raise serializers.ValidationError("Invalid username/password.")
        if not user.is_active:
            raise serializers.ValidationError("User is inactive.")
        self.context["user"] = user
        return data

    def create(self, validated_data):
        auth_login(self.context["request"], self.context["user"])
        return self.context["user"]


class UserSerializer(LinkedModelSerializer):
    password = serializers.CharField(
        required=True, write_only=True, trim_whitespace=False, style={"input_type": "password"}
    )

    def validate_password(self, value):
        password_validation.validate_password(value, self.instance)
        return make_password(value)

    class Meta:
        model = get_user_model()
        fields = [
            "id",
            "href",
            "username",
            "password",
            "is_staff",
            "first_name",
            "last_name",
            "email",
        ]
        immutable_fields = ["username"]


class TournamentSerializer(StateSerializer):
    rounds_href = serializers.SerializerMethodField()
    heats_href = serializers.SerializerMethodField()
    couples_href = serializers.SerializerMethodField()
    adjudicators_href = serializers.SerializerMethodField()
    marks_href = serializers.SerializerMethodField()

    def get_rounds_href(self, obj) -> str:
        return reverse("round-list", kwargs={"tournament_pk": obj.pk})

    def get_heats_href(self, obj) -> str:
        heats_href = reverse("heat-list")
        return replace_query_param(heats_href, "tournament", obj.pk)

    def get_adjudicators_href(self, obj) -> str:
        return reverse("adjudicator-list", kwargs={"tournament_pk": obj.pk})

    def get_couples_href(self, obj) -> str:
        return reverse("couple-list", kwargs={"tournament_pk": obj.pk})

    def get_marks_href(self, obj) -> str:
        marks_href = reverse("mark-list")
        return replace_query_param(marks_href, "tournament", obj.pk)

    def validate_finish(self):
        super().validate_finish()
        if self.instance.rounds.exclude(state=State.FINISHED).exists():
            raise serializers.ValidationError("There are unfinished rounds.")

    class Meta:
        model = Tournament
        fields = "__all__"


class RoundSerializer(StateSerializer):
    parent_lookup_kwargs = {"tournament_pk": "tournament__pk"}

    heats_href = serializers.SerializerMethodField()
    heats = LinkedRelatedField(view_name="heat-detail", read_only=True, many=True)
    couples = CoupleField(read_only=True, many=True)
    dances = serializers.ListField(child=serializers.CharField(max_length=255))
    marks_target = serializers.IntegerField(required=False, min_value=0)

    def get_heats_href(self, obj) -> str:
        heats_href = reverse("heat-list")
        return replace_query_param(heats_href, "rounds", obj.pk)

    def validate_start(self):
        super().validate_start()
        if self.instance.tournament.state != State.STARTED:
            raise serializers.ValidationError("Tournament is not started.")

    class Meta:
        model = Round
        fields = [
            "id",
            "href",
            "state",
            "slug",
            "round_type",
            "heats_href",
            "heats",
            "couples",
            "dances",
            "marks_target",
        ]


class HeatSerializer(LinkedModelSerializer):
    # This serializer must only be used for writing inside the RoundShuffleSerializer

    tournament = LinkedRelatedField(view_name="tournament-detail", read_only=True)
    round = LinkedRelatedField(view_name="round-detail", read_only=True, parent_lookup_kwargs={"tournament_pk": "tournament__pk"})
    couples = CoupleField(scope="round", many=True)
    number = serializers.IntegerField(min_value=1)
    dance = serializers.CharField(allow_blank=False)

    def validate(self, data):
        validated_data = super().validate(data)
        round = self.context.get("round")
        if round:
            validated_data["round"] = round
            if validated_data["dance"] not in round.dances:
                raise serializers.ValidationError(
                    "{dance} is not a valid dance for this round.".format(
                        dance=validated_data["dance"]
                    )
                )

        return validated_data

    class Meta:
        model = Heat
        fields = "__all__"
        immutable_fields = ["round"]


class AdjudicatorSerializer(LinkedModelSerializer):
    parent_lookup_kwargs = {"tournament_pk": "tournament__pk"}

    slug = serializers.SlugField(
        max_length=16, validators=[UniqueForTournamentValidator(queryset=Adjudicator.objects.all())]
    )
    username = serializers.CharField(source="user.username", default=None, read_only=True)

    class Meta:
        model = Adjudicator
        fields = ["id", "href", "slug", "name", "user", "username"]
        immutable_fields = ["slug"]

    def validate(self, data):
        validated_data = super().validate(data)
        tournament = self.context["tournament"]
        validated_data["tournament"] = tournament
        if tournament.state != State.PLANNED:
            raise serializers.ValidationError(
                "Adjudicators cannot be modified, once a tournament is started."
            )
        user = validated_data.get("user")
        if self.instance is None and not validated_data.get("name") and user:
            validated_data["name"] = " ".join(
                (part for part in [user.first_name, user.last_name] if part)
            )
        return validated_data


class CoupleSerializer(LinkedModelSerializer):
    parent_lookup_kwargs = {"tournament_pk": "tournament__pk"}

    class Meta:
        model = Couple
        fields = ["id", "href", "number", "lead", "follow"]
        immutable_fields = ["number"]

    def validate(self, data):
        validated_data = super().validate(data)
        tournament = self.context["tournament"]
        validated_data["tournament"] = tournament
        if tournament.state != State.PLANNED:
            raise serializers.ValidationError(
                "Couples cannot be modified, once a tournament is started."
            )
        return validated_data


class MarkSerializer(LinkedModelSerializer):
    adjudicator = LinkedRelatedField(view_name="adjudicator-detail", read_only=True, parent_lookup_kwargs={"tournament_pk": "tournament__pk"})
    couple = LinkedRelatedField(view_name="couple-detail", read_only=True, parent_lookup_kwargs={"tournament_pk": "tournament__pk"})
    heat = LinkedRelatedField(view_name="heat-detail", read_only=True)

    class Meta:
        model = Mark
        fields = "__all__"
        immutable_fields = ["heat", "couple", "adjudicator"]

    def validate(self, data):
        validated_data = super().validate(data)
        round = validated_data.get("round") or self.instance.heat.round
        if round.state != State.STARTED:
            raise serializers.ValidationError(
                "Marks cannot be modified, if the Round is not started."
            )
        return validated_data


class RoundQualifySerializer(serializers.Serializer):
    couples = CoupleField(many=True, required=False)
    from_round = RoundField(required=False)
    min_marks = serializers.IntegerField(min_value=0, required=False)
    max_marks = serializers.IntegerField(min_value=0, required=False)

    def validate(self, data):
        validated_data = super().validate(data)
        round = self.context["round"]
        if round.state != State.PLANNED:
            raise serializers.ValidationError(
                "Qualification cannot be modified, once a round is started."
            )
        tournament = self.context["tournament"]
        if "couples" not in validated_data:
            if "from_round" in validated_data:
                from_round = validated_data["from_round"]
                qualified_couples = from_round.couples.filter(
                    marks__heat__round=from_round
                ).annotate(round_marks=Sum("marks__value"))
                if "min_marks" in validated_data:
                    qualified_couples = qualified_couples.filter(
                        round_marks__gte=validated_data["min_marks"]
                    )
                if "max_marks" in validated_data:
                    qualified_couples = qualified_couples.filter(
                        round_marks__lte=validated_data["max_marks"]
                    )
                validated_data["couples"] = list(qualified_couples)
            else:
                validated_data["couples"] = list(tournament.couples.all())
        elif "from_round" in validated_data:
            raise serializers.ValidationError(
                "Only one of 'couples' or 'from_round' can be specified."
            )
        return validated_data


class RoundShuffleSerializer(serializers.Serializer):
    num_heats = serializers.IntegerField(min_value=1, write_only=True)
    heats = HeatSerializer(many=True, allow_empty=True, required=False)
    heatlist = serializers.SerializerMethodField()
    force = serializers.BooleanField(default=False, write_only=True)

    def get_heatlist(self, obj) -> List[Dict[str, Union[str, int, List[str]]]]:
        # TODO Revisit the format
        round = self.context["round"]
        return [
            {
                "dance": heat.dance,
                "heat": heat.number,
                "couples": list(heat.couples.values_list("number", flat=True)),
            }
            for heat in round.heats.all()
        ]

    def validate(self, data):
        validated_data = super().validate(data)
        round = self.context["round"]
        num_heats = validated_data["num_heats"]

        if round.state != State.PLANNED:
            raise serializers.ValidationError(
                "Shuffle cannot be modified, once a round is started."
            )
        if not validated_data["force"] and round.heats.all().exists():
            raise serializers.ValidationError("There is already a heat shuffle.")

        if "heats" in validated_data:
            couple_set = list(round.couples.values_list("pk", flat=True).order_by("pk"))
            # Index provided heats
            heat_pool = {
                (heat["dance"], heat["number"]): heat for heat in validated_data.pop("heats")
            }
            validated_data["heats"] = []
            validated_data["heat_couples"] = []
            for dance in round.dances:
                dance_couples = []
                for i in range(num_heats):
                    heat = heat_pool.pop((dance, i + 1), None)
                    if heat is None:
                        raise serializers.ValidationError(
                            "Heat '{dance} {number}' is missing.".format(dance=dance, number=i + 1)
                        )
                    heat_couples = [couple.pk for couple in heat.pop("couples")]
                    dance_couples.extend(heat_couples)
                    validated_data["heats"].append(Heat(**heat))
                    validated_data["heat_couples"].append(heat_couples)
                if sorted(dance_couples) != couple_set:
                    raise serializers.ValidationError(
                        "The selection of couples is incomplete for dance '{dance}'.".format(
                            dance=dance
                        )
                    )
            if heat_pool:
                raise serializers.ValidationError("Superfluous heats were provided.")

        else:
            couple_pks = list(round.couples.values_list("pk", flat=True))
            validated_data["heats"] = []
            validated_data["heat_couples"] = []
            for dance in round.dances:
                random.shuffle(couple_pks)
                for i in range(num_heats):
                    validated_data["heats"].append(Heat(round=round, number=i + 1, dance=dance))
                    validated_data["heat_couples"].append(couple_pks[i::num_heats])
        return validated_data


class RoundResultSerializer(serializers.Serializer):
    state = serializers.CharField(read_only=True)
    marks = serializers.SerializerMethodField()

    def get_marks(self, obj) -> Dict[str, int]:
        # TODO Revisit the format
        round = self.context["round"]
        return {
            couple.number: Mark.objects.filter(heat__in=round.heats.all(), couple=couple).aggregate(
                Sum("value")
            )["value__sum"]
            for couple in round.couples.all()
        }
