import pytest
from django.urls import reverse


def test_crud_tournament(admin_client):
    response = admin_client.post(
        reverse("tournament-list"),
        data={"name": "Test Tournament"},
        content_type="application/json",
    )
    assert response.status_code == 201

    response = admin_client.post(
        reverse("tournament-list"),
        data={"name": "Test Tournament"},
        content_type="application/json",
    )
    assert response.status_code == 400

    response = admin_client.get(reverse("tournament-list"))
    assert response.status_code == 200
    tournament = next((t for t in response.json()["results"] if t["name"] == "Test Tournament"))

    response = admin_client.get(tournament["href"])
    assert response.status_code == 200
    tournament = response.json()
    assert tournament["description"] == ""
    assert tournament["state"] == "planned"

    response = admin_client.patch(
        tournament["href"],
        data={"description": "For unit testing."},
        content_type="application/json",
    )
    assert response.status_code == 200
    assert response.json()["description"] == "For unit testing."

    response = admin_client.delete(tournament["href"])
    assert response.status_code == 204

    response = admin_client.get(tournament["href"])
    assert response.status_code == 404


@pytest.mark.asyncio
async def test_tournament_state(tournament, async_admin_client, tournament_communicator):
    assert await tournament_communicator.receive_nothing()

    response = await async_admin_client.get(
        reverse("tournament-detail", kwargs={"pk": tournament.pk})
    )
    assert await tournament_communicator.receive_nothing()

    assert response.json()["state"] == "planned"
    response = (
        await async_admin_client.post(reverse("tournament-start", kwargs={"pk": tournament.pk}))
    ).json()
    message = await tournament_communicator.receive_json_from()
    assert response["state"] == "started"
    assert message["action"] == "update"
    assert response == message["data"]

    response = (
        await async_admin_client.post(reverse("tournament-finish", kwargs={"pk": tournament.pk}))
    ).json()
    message = await tournament_communicator.receive_json_from()
    assert response["state"] == "finished"
    assert message["action"] == "update"
    assert response == message["data"]

    response = (
        await async_admin_client.post(reverse("tournament-reset", kwargs={"pk": tournament.pk}))
    ).json()
    message = await tournament_communicator.receive_json_from()
    assert response["state"] == "planned"
    assert message["action"] == "update"
    assert response == message["data"]

    assert await tournament_communicator.receive_nothing()
