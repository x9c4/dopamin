import pytest


@pytest.mark.asyncio
async def test_ws_connect(notification_communicator):
    assert await notification_communicator.receive_nothing()
    await notification_communicator.send_json_to({"type": "notification", "data": "TEST"})
    assert await notification_communicator.receive_nothing()
    await notification_communicator.send_json_to({"type": "notification", "title": "TEST"})
    response = await notification_communicator.receive_json_from()
    assert response == {
        "type": "notification",
        "title": "TEST",
        "timeout": 10000,
        "variant": "info",
    }
    assert await notification_communicator.receive_nothing()


@pytest.mark.asyncio
async def test_ws_couple_add(tournament, tournament_communicator):
    await tournament.couples.acreate(number="12", lead="A", follow="B")
    response = await tournament_communicator.receive_json_from()
    assert response["action"] == "create"
    assert response["data"]["lead"] == "A"
    assert response["data"]["follow"] == "B"
    assert response["data"]["href"].startswith(f"/api/v1/tournament/{tournament.pk}/")
