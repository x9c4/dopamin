import pytest

from django.urls import reverse


def test_round_result_empty(admin_client, tournament):
    round = tournament.rounds.create(slug="Round 1")
    url = reverse("round-results", kwargs={"tournament_pk": tournament.pk, "pk": round.pk})
    response = admin_client.get(url)
    assert response.status_code == 200
    assert response.data == {"state": "planned", "marks": {}}


def test_round_result_no_mark(admin_client, tournament):
    adjudicator = tournament.adjudicators.create(slug="AB")
    couple = tournament.couples.create(number="12")
    round = tournament.rounds.create(slug="Round 1")
    round.couples.add(couple)
    url = reverse("round-results", kwargs={"tournament_pk": tournament.pk, "pk": round.pk})
    response = admin_client.get(url)
    assert response.status_code == 200
    assert response.data == {"state": "planned", "marks": {"12": None}}

    round.start()
    response = admin_client.get(url)
    assert response.status_code == 200
    assert response.data == {"state": "started", "marks": {"12": None}}

    round.finish()
    response = admin_client.get(url)
    assert response.status_code == 200
    assert response.data == {"state": "finished", "marks": {"12": None}}


def test_round_result_mark(admin_client, tournament):
    adjudicator = tournament.adjudicators.create(slug="AB")
    couple = tournament.couples.create(number="12")
    couple_url = reverse("couple-detail", kwargs={"tournament_pk": tournament.pk, "pk": couple.pk})
    round = tournament.rounds.create(slug="Round 1", dances=["aa", "bb"])
    round.couples.add(couple)
    round_url = reverse("round-results", kwargs={"tournament_pk": tournament.pk, "pk": round.pk})
    shuffle_url = reverse("round-shuffle", kwargs={"tournament_pk": tournament.pk, "pk": round.pk})
    response = admin_client.post(shuffle_url, {"num_heats": 1})
    assert response.status_code == 200
    assert response.data["heatlist"] == [
        {"couples": ["12"], "dance": "aa", "heat": 1},
        {"couples": ["12"], "dance": "bb", "heat": 1},
    ]

    response = admin_client.get(round_url)
    assert response.status_code == 200
    assert response.data == {"state": "planned", "marks": {"12": 0}}

    round.start()
    response = admin_client.get(round_url)
    assert response.status_code == 200
    assert response.data == {"state": "started", "marks": {"12": 0}}

    round.finish()
    response = admin_client.get(round_url)
    assert response.status_code == 200
    assert response.data == {"state": "finished", "marks": {"12": 0}}
