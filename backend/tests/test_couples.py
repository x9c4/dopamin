from django.urls import reverse


def test_crud_couples(tournament, admin_client):
    assert not tournament.couples.exists()
    couples_url = reverse("couple-list", kwargs={"tournament_pk": tournament.pk})
    response = admin_client.get(couples_url)
    assert response.status_code == 200
    assert response.data["results"] == []

    response = admin_client.post(
        couples_url, {"number": "42", "lead": "lead1", "follow": "follow1"}
    )
    assert response.status_code == 201
    assert response.data["href"].startswith(couples_url)
    assert response.data["number"] == "42"
    assert response.data["lead"] == "lead1"
    assert response.data["follow"] == "follow1"
