from backend.skating import SkatingReport


def test_empty_skating_report():
    dances = ["SW", "TG", "QS"]
    couples = ["11", "21", "31", "41", "51", "61"]
    adjudicators = ["A", "B", "C", "D", "E"]
    report = SkatingReport(dances, couples, adjudicators)
    assert report.dance_report("SW")["41"]["marks"] == [None] * 5
    assert report.dance_report("QS")["61"]["marks"] == [None] * 5
    assert report.dance_report("TG")["31"]["majorities"] == [None] * 6
    assert report.dance_report("SW")["21"]["placement"] == "None"


def test_example_A():
    dances = ["SW"]
    couples = ["11", "21", "31", "41", "51", "61"]
    adjudicators = ["A", "B", "C", "D", "E"]
    report = SkatingReport(dances, couples, adjudicators)
    report.mark("SW", "A", [1, 2, 3, 4, 5, 6])
    report.mark("SW", "B", [5, 2, 3, 4, 1, 6])
    report.mark("SW", "C", [1, 5, 3, 2, 4, 6])
    report.mark("SW", "D", [1, 4, 2, 3, 5, 6])
    report.mark("SW", "E", [2, 1, 3, 4, 5, 6])

    report.evaluate()

    dance_report = report.dance_report("SW")
    assert dance_report["21"]["marks"] == [2, 2, 5, 4, 1]
    assert dance_report["11"]["placement"] == "1"
    assert dance_report["21"]["placement"] == "2"
    assert dance_report["31"]["placement"] == "3"
    assert dance_report["41"]["placement"] == "4"
    assert dance_report["51"]["placement"] == "5"
    assert dance_report["61"]["placement"] == "6"
    assert dance_report["21"]["majorities"] == [(1, 1), (3, 5), None, None, None, None]


def test_example_B():
    dances = ["SW"]
    couples = ["12", "22", "32", "42", "52", "62"]
    adjudicators = ["A", "B", "C", "D", "E"]
    report = SkatingReport(dances, couples, adjudicators)
    report.mark("SW", "A", [1, 3, 2, 4, 5, 6])
    report.mark("SW", "B", [1, 2, 5, 3, 4, 6])
    report.mark("SW", "C", [1, 2, 5, 4, 3, 6])
    report.mark("SW", "D", [4, 1, 2, 5, 3, 6])
    report.mark("SW", "E", [4, 1, 2, 3, 5, 6])

    report.evaluate()

    dance_report = report.dance_report("SW")
    assert dance_report["12"]["placement"] == "1"
    assert dance_report["22"]["placement"] == "2"
    assert dance_report["32"]["placement"] == "3"
    assert dance_report["42"]["placement"] == "4"
    assert dance_report["52"]["placement"] == "5"
    assert dance_report["62"]["placement"] == "6"
    assert dance_report["42"]["majorities"] == [None, None, (2, 6), (4, 14), None, None]
    assert dance_report["52"]["majorities"] == [None, None, (2, 6), (3, 10), None, None]


def test_example_C():
    dances = ["SW"]
    couples = ["13", "23", "33", "43", "53", "63"]
    adjudicators = ["A", "B", "C", "D", "E"]
    report = SkatingReport(dances, couples, adjudicators)
    report.mark("SW", "A", [1, 2, 5, 3, 4, 6])
    report.mark("SW", "B", [1, 2, 5, 3, 4, 6])
    report.mark("SW", "C", [1, 5, 2, 4, 3, 6])
    report.mark("SW", "D", [5, 1, 2, 6, 3, 4])
    report.mark("SW", "E", [5, 4, 2, 1, 3, 6])

    report.evaluate()

    dance_report = report.dance_report("SW")
    assert dance_report["13"]["placement"] == "1"
    assert dance_report["23"]["placement"] == "2"
    assert dance_report["33"]["placement"] == "3"
    assert dance_report["43"]["placement"] == "4"
    assert dance_report["53"]["placement"] == "5"
    assert dance_report["63"]["placement"] == "6"
    assert dance_report["23"]["majorities"] == [(1, 1), (3, 5), None, None, None, None]
    assert dance_report["33"]["majorities"] == [None, (3, 6), None, None, None, None]
    assert dance_report["43"]["majorities"] == [(1, 1), (1, 1), (3, 7), None, None, None]
    assert dance_report["53"]["majorities"] == [None, None, (3, 9), None, None, None]


def test_example_D():
    dances = ["SW"]
    couples = ["14", "24", "34", "44", "54", "64"]
    adjudicators = ["A", "B", "C", "D", "E"]
    report = SkatingReport(dances, couples, adjudicators)
    report.mark("SW", "A", [2, 1, 5, 3, 4, 6])
    report.mark("SW", "B", [1, 2, 6, 3, 4, 5])
    report.mark("SW", "C", [5, 2, 1, 3, 4, 6])
    report.mark("SW", "D", [1, 5, 2, 3, 6, 4])
    report.mark("SW", "E", [1, 5, 2, 6, 4, 3])

    report.evaluate()

    dance_report = report.dance_report("SW")
    assert dance_report["14"]["placement"] == "1"
    assert dance_report["24"]["placement"] == "2"
    assert dance_report["34"]["placement"] == "3"
    assert dance_report["44"]["placement"] == "4"
    assert dance_report["54"]["placement"] == "5"
    assert dance_report["64"]["placement"] == "6"
    assert dance_report["24"]["majorities"] == [(1, 1), (3, 5), (3, 5), (3, 5), (5, 15), None]
    assert dance_report["34"]["majorities"] == [(1, 1), (3, 5), (3, 5), (3, 5), (4, 10), None]


def test_example_D2():
    dances = ["SW"]
    couples = ["14", "24", "34", "44", "54", "64"]
    adjudicators = ["A", "B", "C", "D", "E"]
    report = SkatingReport(dances, couples, adjudicators)
    report.mark("SW", "A", [2, 1, 5, 3, 4, 6])
    report.mark("SW", "B", [1, 2, 5, 3, 4, 6])
    report.mark("SW", "C", [5, 2, 1, 3, 4, 6])
    report.mark("SW", "D", [1, 5, 2, 3, 6, 4])
    report.mark("SW", "E", [1, 5, 2, 6, 4, 3])

    report.evaluate()

    dance_report = report.dance_report("SW")
    assert dance_report["14"]["placement"] == "1"
    assert dance_report["24"]["placement"] == "2-3"
    assert dance_report["34"]["placement"] == "2-3"
    assert dance_report["44"]["placement"] == "4"
    assert dance_report["54"]["placement"] == "5"
    assert dance_report["64"]["placement"] == "6"
    assert dance_report["24"]["majorities"] == [(1, 1), (3, 5), (3, 5), (3, 5), (5, 15), (5, 15)]
    assert dance_report["34"]["majorities"] == [(1, 1), (3, 5), (3, 5), (3, 5), (5, 15), (5, 15)]


def test_example_E():
    dances = ["SW"]
    couples = ["15", "25", "35", "45", "55", "65"]
    adjudicators = ["A", "B", "C", "D", "E"]
    report = SkatingReport(dances, couples, adjudicators)
    report.mark("SW", "A", [1, 6, 2, 4, 5, 3])
    report.mark("SW", "B", [1, 5, 4, 2, 6, 3])
    report.mark("SW", "C", [3, 4, 1, 5, 2, 6])
    report.mark("SW", "D", [2, 1, 5, 6, 3, 4])
    report.mark("SW", "E", [3, 1, 5, 2, 4, 6])

    report.evaluate()

    dance_report = report.dance_report("SW")
    assert dance_report["15"]["placement"] == "1"
    assert dance_report["25"]["placement"] == "2"
    assert dance_report["35"]["placement"] == "3"
    assert dance_report["45"]["placement"] == "4"
    assert dance_report["55"]["placement"] == "5"
    assert dance_report["65"]["placement"] == "6"
    assert dance_report["15"]["majorities"] == [(2, 2), (3, 4), None, None, None, None]
    assert dance_report["25"]["majorities"] == [(2, 2), (2, 2), (2, 2), (3, 6), None, None]
    assert dance_report["35"]["majorities"] == [(1, 1), (2, 3), (2, 3), (3, 7), None, None]
    assert dance_report["45"]["majorities"] == [None, (2, 4), (2, 4), (3, 8), None, None]
    assert dance_report["55"]["majorities"] == [None, (1, 2), (2, 5), (3, 9), None, None]
    assert dance_report["65"]["majorities"] == [None, None, (2, 6), (3, 10), None, None]


def test_example_M():
    dances = ["SW", "TG", "VW", "SF", "QS"]
    couples = ["25", "35", "45", "55", "65", "75"]
    adjudicators = ["A", "B", "C", "D", "E"]
    report = SkatingReport(dances, couples, adjudicators)

    report.mark("SW", "A", [1, 2, 4, 3, 6, 5])
    report.mark("SW", "B", [1, 2, 5, 3, 6, 4])
    report.mark("SW", "C", [2, 5, 3, 6, 1, 4])
    report.mark("SW", "D", [5, 2, 1, 3, 4, 6])
    report.mark("SW", "E", [3, 5, 1, 6, 4, 2])

    report.mark("TG", "A", [1, 5, 3, 4, 2, 6])
    report.mark("TG", "B", [2, 3, 1, 4, 5, 6])
    report.mark("TG", "C", [2, 1, 3, 4, 5, 6])
    report.mark("TG", "D", [3, 2, 1, 4, 5, 6])
    report.mark("TG", "E", [4, 2, 3, 1, 5, 6])

    report.mark("VW", "A", [1, 3, 5, 2, 4, 6])
    report.mark("VW", "B", [1, 2, 3, 6, 4, 5])
    report.mark("VW", "C", [2, 1, 3, 6, 5, 4])
    report.mark("VW", "D", [2, 1, 6, 3, 5, 4])
    report.mark("VW", "E", [3, 2, 1, 4, 6, 5])

    report.mark("SF", "A", [2, 1, 5, 3, 6, 4])
    report.mark("SF", "B", [2, 1, 6, 3, 4, 5])
    report.mark("SF", "C", [5, 2, 3, 1, 6, 4])
    report.mark("SF", "D", [1, 5, 2, 3, 6, 4])
    report.mark("SF", "E", [5, 2, 3, 6, 4, 1])

    report.mark("QS", "A", [2, 1, 3, 5, 4, 6])
    report.mark("QS", "B", [6, 5, 2, 1, 4, 3])
    report.mark("QS", "C", [2, 1, 4, 5, 6, 3])
    report.mark("QS", "D", [6, 5, 2, 1, 3, 4])
    report.mark("QS", "E", [1, 2, 4, 3, 6, 5])

    report.evaluate()

    dance_report = report.dance_report("SW")
    assert [dance_report[couple]["placement"]for couple in couples] == [
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
    ]

    dance_report = report.dance_report("TG")
    assert [dance_report[couple]["placement"]for couple in couples] == [
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
    ]

    dance_report = report.dance_report("VW")
    assert [dance_report[couple]["placement"] for couple in couples] == [
        "1-2",
        "1-2",
        "3",
        "4",
        "5-6",
        "5-6",
    ]

    dance_report = report.dance_report("SF")
    assert [dance_report[couple]["placement"] for couple in couples] == [
        "2",
        "1",
        "4",
        "3",
        "6",
        "5",
    ]

    dance_report = report.dance_report("QS")
    assert [dance_report[couple]["placement"] for couple in couples] == [
        "2",
        "1",
        "4",
        "3",
        "6",
        "5",
    ]

    final_report = report.final_report()

    assert final_report["25"]["sum"] == 7.5
    assert final_report["35"]["sum"] == 7.5
    assert final_report["45"]["sum"] == 17.0
    assert final_report["55"]["sum"] == 18.0
    assert final_report["65"]["sum"] == 27.5
    assert final_report["75"]["sum"] == 27.5

    assert final_report["25"]["rule11"] == [(7, 7), (16, 25), None, None, None, None]
    assert final_report["35"]["rule11"] == [(7, 7), (17, 27), None, None, None, None]
    assert final_report["65"]["rule11"] == [None, None, None, None, (17, 68), None]
    assert final_report["75"]["rule11"] == [None, None, None, None, (17, 66), None]

    assert final_report["25"]["placement"] == "2"
    assert final_report["35"]["placement"] == "1"
    assert final_report["45"]["placement"] == "3"
    assert final_report["55"]["placement"] == "4"
    assert final_report["65"]["placement"] == "6"
    assert final_report["75"]["placement"] == "5"


def test_example_N():
    dances = ["SW", "TG", "VW", "SF", "QS"]
    couples = ["26", "36", "46", "56", "66", "76"]
    adjudicators = ["A", "B", "C", "D", "E"]
    report = SkatingReport(dances, couples, adjudicators)

    report.mark("SW", "A", [1, 2, 3, 4, 5, 6])
    report.mark("SW", "B", [1, 2, 3, 5, 6, 4])
    report.mark("SW", "C", [2, 5, 6, 3, 1, 4])
    report.mark("SW", "D", [5, 2, 3, 1, 4, 6])
    report.mark("SW", "E", [3, 5, 6, 1, 4, 2])

    report.mark("TG", "A", [1, 5, 4, 3, 2, 6])
    report.mark("TG", "B", [2, 3, 4, 1, 5, 6])
    report.mark("TG", "C", [2, 1, 4, 3, 5, 6])
    report.mark("TG", "D", [3, 2, 4, 1, 5, 6])
    report.mark("TG", "E", [4, 2, 1, 3, 5, 6])

    report.mark("VW", "A", [1, 2, 3, 4, 5, 6])
    report.mark("VW", "B", [1, 3, 4, 5, 2, 6])
    report.mark("VW", "C", [1, 4, 5, 6, 2, 3])
    report.mark("VW", "D", [1, 5, 6, 2, 3, 4])
    report.mark("VW", "E", [1, 6, 2, 3, 4, 5])

    report.mark("SF", "A", [1, 5, 2, 3, 4, 6])
    report.mark("SF", "B", [1, 6, 2, 3, 5, 4])
    report.mark("SF", "C", [2, 3, 5, 1, 4, 6])
    report.mark("SF", "D", [5, 2, 1, 3, 4, 6])
    report.mark("SF", "E", [2, 3, 5, 6, 1, 4])

    report.mark("QS", "A", [1, 3, 2, 5, 6, 4])
    report.mark("QS", "B", [5, 2, 6, 1, 3, 4])
    report.mark("QS", "C", [1, 4, 2, 5, 3, 6])
    report.mark("QS", "D", [5, 2, 6, 1, 4, 3])
    report.mark("QS", "E", [2, 4, 1, 3, 5, 6])

    report.evaluate()

    dance_report = report.dance_report("SW")
    assert [dance_report[couple]["placement"] for couple in couples] == [
        "1",
        "2",
        "4",
        "3",
        "5",
        "6",
    ]

    dance_report = report.dance_report("TG")
    assert [dance_report[couple]["placement"] for couple in couples] == [
        "1",
        "2",
        "4",
        "3",
        "5",
        "6",
    ]

    dance_report = report.dance_report("VW")
    assert [dance_report[couple]["placement"] for couple in couples] == [
        "1",
        "3-5",
        "3-5",
        "3-5",
        "2",
        "6",
    ]

    dance_report = report.dance_report("SF")
    assert [dance_report[couple]["placement"] for couple in couples] == [
        "1",
        "4",
        "2",
        "3",
        "5",
        "6",
    ]

    dance_report = report.dance_report("QS")
    assert [dance_report[couple]["placement"] for couple in couples] == [
        "1",
        "4",
        "2",
        "3",
        "5",
        "6",
    ]

    final_report = report.final_report()

    assert final_report["26"]["sum"] == 5.0
    assert final_report["36"]["sum"] == 16.0
    assert final_report["46"]["sum"] == 16.0
    assert final_report["56"]["sum"] == 16.0
    assert final_report["66"]["sum"] == 22.0
    assert final_report["76"]["sum"] == 30.0

    assert final_report["36"]["rule11"] == [None, (10, 19), (15, 34), None, None, None]
    assert final_report["46"]["rule11"] == [None, (8, 13), (12, 25), None, None, None]

    assert final_report["26"]["placement"] == "1"
    assert final_report["36"]["placement"] == "2"
    assert final_report["46"]["placement"] == "3"
    assert final_report["56"]["placement"] == "4"
    assert final_report["66"]["placement"] == "5"
    assert final_report["76"]["placement"] == "6"


def test_example_P():
    dances = ["SW", "TG", "VW", "SF", "QS"]
    couples = ["27", "37", "47", "57", "67", "77"]
    adjudicators = ["A", "B", "C", "D", "E"]
    report = SkatingReport(dances, couples, adjudicators)

    report.mark("SW", "A", [2, 3, 4, 1, 6, 5])
    report.mark("SW", "B", [2, 3, 5, 1, 6, 4])
    report.mark("SW", "C", [5, 6, 3, 2, 1, 4])
    report.mark("SW", "D", [2, 3, 1, 5, 4, 6])
    report.mark("SW", "E", [5, 6, 1, 3, 4, 2])

    report.mark("TG", "A", [1, 3, 2, 4, 6, 5])
    report.mark("TG", "B", [2, 1, 5, 4, 6, 3])
    report.mark("TG", "C", [2, 3, 5, 4, 6, 1])
    report.mark("TG", "D", [3, 1, 5, 4, 6, 2])
    report.mark("TG", "E", [4, 3, 5, 1, 6, 2])

    report.mark("VW", "A", [1, 2, 3, 4, 5, 6])
    report.mark("VW", "B", [1, 3, 4, 5, 2, 6])
    report.mark("VW", "C", [1, 4, 5, 6, 2, 3])
    report.mark("VW", "D", [1, 5, 6, 2, 3, 4])
    report.mark("VW", "E", [1, 6, 2, 3, 4, 5])

    report.mark("SF", "A", [3, 1, 4, 5, 2, 6])
    report.mark("SF", "B", [3, 1, 5, 6, 2, 4])
    report.mark("SF", "C", [1, 2, 4, 3, 5, 6])
    report.mark("SF", "D", [3, 5, 4, 2, 1, 6])
    report.mark("SF", "E", [6, 2, 1, 3, 5, 4])

    report.mark("QS", "A", [2, 4, 1, 6, 5, 3])
    report.mark("QS", "B", [6, 4, 5, 3, 1, 2])
    report.mark("QS", "C", [2, 6, 1, 3, 5, 4])
    report.mark("QS", "D", [6, 3, 5, 4, 1, 2])
    report.mark("QS", "E", [1, 6, 2, 5, 4, 3])

    report.evaluate()

    dance_report = report.dance_report("SW")
    assert [dance_report[couple]["placement"] for couple in couples] == [
        "2",
        "4",
        "3",
        "1",
        "5",
        "6",
    ]

    dance_report = report.dance_report("TG")
    assert [dance_report[couple]["placement"] for couple in couples] == [
        "1",
        "3",
        "5",
        "4",
        "6",
        "2",
    ]

    dance_report = report.dance_report("VW")
    assert [dance_report[couple]["placement"] for couple in couples] == [
        "1",
        "3-5",
        "3-5",
        "3-5",
        "2",
        "6",
    ]

    dance_report = report.dance_report("SF")
    assert [dance_report[couple]["placement"] for couple in couples] == [
        "3",
        "1",
        "5",
        "4",
        "2",
        "6",
    ]

    dance_report = report.dance_report("QS")
    assert [dance_report[couple]["placement"] for couple in couples] == [
        "2",
        "6",
        "1",
        "5",
        "4",
        "3",
    ]

    final_report = report.final_report()

    assert final_report["27"]["sum"] == 9.0
    assert final_report["37"]["sum"] == 18.0
    assert final_report["47"]["sum"] == 18.0
    assert final_report["57"]["sum"] == 18.0
    assert final_report["67"]["sum"] == 19.0
    assert final_report["77"]["sum"] == 23.0

    assert final_report["37"]["rule11"] == [None, (7, 10), (15, 34), None, None, None]
    assert final_report["47"]["rule11"] == [None, (8, 11), (10, 17), None, None, None]
    assert final_report["57"]["rule11"] == [None, (6, 9), (12, 27), None, None, None]

    assert final_report["27"]["placement"] == "1"
    assert final_report["37"]["placement"] == "2"
    assert final_report["47"]["placement"] == "3"
    assert final_report["57"]["placement"] == "4"
    assert final_report["67"]["placement"] == "5"
    assert final_report["77"]["placement"] == "6"
