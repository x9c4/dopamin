import pytest
import pytest_asyncio

from django.urls import path
from channels.routing import URLRouter
from channels.testing import WebsocketCommunicator

from backend.models import Tournament
from backend.consumers import NotificationConsumer, TournamentConsumer


@pytest_asyncio.fixture
async def async_admin_client(admin_user, async_client):
    await async_client.aforce_login(admin_user)
    return async_client


@pytest.fixture
def tournament(transactional_db):
    """A Tournamet for the test"""
    # TODO check if (after making everything async) this can be non transactional.
    return Tournament.objects.create(name="Test tournament")


@pytest_asyncio.fixture
async def notification_communicator():
    communicator = WebsocketCommunicator(NotificationConsumer.as_asgi(), "/testws/")
    connected, subprotocol = await communicator.connect()
    assert connected, "Notification communicator failed to connect."
    yield communicator
    await communicator.disconnect()


@pytest_asyncio.fixture
async def tournament_communicator(tournament):
    application = URLRouter([path("tournament_ws/<id>/", TournamentConsumer.as_asgi())])
    communicator = WebsocketCommunicator(application, f"/tournament_ws/{tournament.pk}/")
    connected, subprotocol = await communicator.connect()
    assert connected, "Tournament communicator failed to connect."
    yield communicator
    await communicator.disconnect()
