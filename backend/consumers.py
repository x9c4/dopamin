from logging import getLogger

from channels.generic.websocket import AsyncJsonWebsocketConsumer

logger = getLogger(__name__)


class NotificationConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        # self.channel = self.scope["url_route"]["kwargs"]["channel"]
        await self.accept()
        await self.channel_layer.group_add("notification", self.channel_name)

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard("notification", self.channel_name)

    async def receive_json(self, data):
        # Message from client
        if data.get("type") == "notification":
            title = data.get("title")
            if title:
                await self.channel_layer.group_send(
                    "notification",
                    {
                        "type": "notification",
                        "title": title,
                        "timeout": 10000,
                        "variant": "info",
                    },
                )

    async def notification(self, event):
        # Group notification
        print(event)
        notification = {
            "type": "notification",
            "title": event.get("title"),
            "variant": event.get("variant"),
            "timeout": event.get("timeout"),
        }
        await self.send_json(notification)


class TournamentConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        self.id = int(self.scope["url_route"]["kwargs"]["id"])
        await self.accept()
        await self.channel_layer.group_add("tournament", self.channel_name)

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard("tournament", self.channel_name)

    async def update(self, event):
        logger.debug("Group notification '%s'", event)
        if self.id == event.get("tournament_id"):
            notification = {
                "action": event.get("action"),
                "tournament_id": self.id,
                "model": event.get("model"),
                "data": event.get("data"),
            }
            await self.send_json(notification)
