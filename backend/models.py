from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.conf import settings
from django.db import models
from django.db.transaction import atomic

from backend.skating import SkatingReport

channel_layer = get_channel_layer()
group_send = async_to_sync(channel_layer.group_send)


class State(models.TextChoices):
    PLANNED = "planned", "planned"
    STARTED = "started", "started"
    FINISHED = "finished", "finished"


class StateModel(models.Model):
    state = models.CharField(
        max_length=16, choices=State.choices, default=State.PLANNED, null=False
    )

    @atomic
    def reset(self):
        if self.state != State.PLANNED:
            self.state = State.PLANNED
            self.save()
            group_send(
                "notification",
                {
                    "type": "notification",
                    "title": f"{self} reset.",
                    "variant": "warning",
                },
            )

    @atomic
    def start(self):
        if self.state != State.PLANNED:
            raise Exception("Cannot be started.")
        self.state = State.STARTED
        self.save()
        group_send(
            "notification",
            {
                "type": "notification",
                "title": f"{self} started.",
                "variant": "info",
            },
        )

    @atomic
    def finish(self):
        if self.state != State.STARTED:
            raise Exception("Cannot be finished.")
        self.state = State.FINISHED
        self.save()
        group_send(
            "notification",
            {
                "type": "notification",
                "title": f"{self} finished.",
                "variant": "info",
            },
        )

    class Meta:
        abstract = True


class Tournament(StateModel):
    name = models.CharField(max_length=255, blank=False, null=False, unique=True)
    description = models.TextField(blank=True, null=False)
    date = models.DateField(null=True)

    def __str__(self):
        return f"Tournament {self.name}"

    @atomic
    def reset(self):
        for round in self.rounds.all():
            round.reset()
        super().reset()


class Couple(models.Model):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE, related_name="couples")
    lead = models.CharField(max_length=255, blank=True, null=False)
    follow = models.CharField(max_length=255, blank=True, null=False)
    number = models.SlugField(max_length=16, blank=False, null=False)
    # external_reference = model.CharField(max_length=255, blank=True, null=False, index=True)
    # placement_from = models.PositiveIntegerField(null=True)
    # placement_to = models.PositiveIntegerField(null=True)
    # points = models.PositiveIntegerField(null=True)

    class Meta:
        unique_together = (("tournament", "number"),)


class Round(StateModel):
    class RoundType(models.TextChoices):
        QUALIFICATION = "quali", "Qualification Round"
        PRELIMINARY = "prelim", "Preliminary Round"
        REDANCE = "redance", "Redance"
        FINAL = "final", "Final"

    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE, related_name="rounds")
    couples = models.ManyToManyField(Couple, related_name="rounds")
    slug = models.SlugField(max_length=16, blank=False, null=False)
    round_type = models.CharField(
        max_length=8, choices=RoundType.choices, default=RoundType.PRELIMINARY, null=False
    )
    dances = models.JSONField(default=list)
    marks_target = models.IntegerField(null=True)
    report = models.JSONField(default=None, null=True)

    def __str__(self):
        return f"{self.round_type} {self.slug}"

    @atomic
    def reset(self):
        report = None
        super().reset()

    @atomic
    def shuffle(self, heats, heat_couples, force=True):
        if self.state != State.PLANNED:
            raise Exception("Round is not in planning state.")
        if self.heats.exists():
            if force:
                self.heats.all().delete()
            else:
                raise Exception("There is already a heat shuffle.")

        for heat, couples in zip(heats, heat_couples):
            heat.save()
            heat.couples.set(couples)
        for heat in self.heats.all():
            for adjudicator in self.tournament.adjudicators.all():
                for couple in heat.couples.all():
                    Mark.objects.get_or_create(
                        heat=heat, couple=couple, adjudicator=adjudicator, defaults={"value": 0}
                    )

    def envaluate_final(self):
        if self.round_type != self.RoundType.FINAL:
            raise Exception("Round is not a final.")
        if self.state != State.FINISHED:
            raise Exception("Round is unfinished.")
        couples = list(self.couples.order_by("number"))
        adjudicators = list(self.tournament.adjudicators.all())
        skating_report = SkatingReport(
            self.dances, [couple.number for couple in couples], [adj.slug for adj in adjudicators]
        )
        for adj in adjudicators:
            for dance in self.dances:
                marks = list(
                    Mark.objects.filter(heat__in=self.heats.filter(dance=dance), adjudicator=adj)
                    .order_by("couple__number")
                    .values_list("value", flat=True)
                )
                skating_report.mark(dance, adj.slug, marks)
        skating_report.evaluate()
        self.report = {
            "dances": {
                dance: skating_report.dance_report(dance) for dance in self.dances
            },
            "final": skating_report.final_report(),
        }
        self.save(update_fields="report")

    class Meta:
        unique_together = (("tournament", "round_type", "slug"),)


class Heat(models.Model):
    round = models.ForeignKey(Round, on_delete=models.CASCADE, related_name="heats")
    couples = models.ManyToManyField(Couple, related_name="heats")
    number = models.IntegerField(null=False)
    dance = models.CharField(max_length=255, blank=False, null=False)

    @property
    def tournament(self):
        return self.round.tournament

    def __str__(self):
        return f"{self.dance} heat {self.number}"

    class Meta:
        unique_together = (("round", "number", "dance"),)


class Adjudicator(models.Model):
    tournament = models.ForeignKey(
        Tournament, on_delete=models.CASCADE, related_name="adjudicators"
    )
    slug = models.SlugField(max_length=16, blank=False, null=False)
    name = models.CharField(max_length=255, blank=True, null=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)

    class Meta:
        unique_together = (("tournament", "slug"),)


class Mark(models.Model):
    adjudicator = models.ForeignKey(Adjudicator, on_delete=models.CASCADE, related_name="marks")
    couple = models.ForeignKey(Couple, on_delete=models.CASCADE, related_name="marks")
    heat = models.ForeignKey(Heat, on_delete=models.CASCADE, related_name="marks")
    value = models.IntegerField(null=False)
    remark = models.IntegerField(null=False, default=0)

    class Meta:
        unique_together = (("adjudicator", "couple", "heat"),)
