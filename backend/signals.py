from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.db import transaction
from django.db.models.signals import m2m_changed, post_save, pre_delete
from django.dispatch import receiver

from .models import Adjudicator, Couple, Heat, Mark, Round, Tournament
from .serializers import (
    AdjudicatorSerializer,
    CoupleSerializer,
    HeatSerializer,
    MarkSerializer,
    RoundSerializer,
    TournamentSerializer,
)

channel_layer = get_channel_layer()
group_send = async_to_sync(channel_layer.group_send)


@receiver([post_save, pre_delete], sender=Tournament)
def tournament_update(sender, instance, created=None, **kwargs):
    action = "create" if created else "update" if created is False else "delete"
    serializer = TournamentSerializer(instance=instance)
    data = serializer.data

    transaction.on_commit(
        lambda: group_send(
            "tournament",
            {
                "type": "update",
                "action": action,
                "tournament_id": instance.pk,
                "model": "Tournament",
                "id": instance.pk,
                "data": data,
            },
        )
    )


@receiver([post_save, pre_delete], sender=Round)
def round_update(sender, instance, created=None, **kwargs):
    action = "create" if created else "update" if created is False else "delete"
    serializer = RoundSerializer(instance=instance)
    data = serializer.data

    transaction.on_commit(
        lambda: group_send(
            "tournament",
            {
                "type": "update",
                "action": action,
                "tournament_id": instance.tournament_id,
                "model": "Round",
                "id": instance.pk,
                "data": data,
            },
        )
    )


@receiver(m2m_changed, sender=Round.couples.through)
def update_round_couple(sender, action, instance, pk_set, **kwargs):
    if action in ["post_add", "post_remove"] and pk_set:
        serializer = RoundSerializer(instance=instance)
        data = serializer.data

        transaction.on_commit(
            lambda: group_send(
                "tournament",
                {
                    "type": "update",
                    "action": "update",
                    "tournament_id": instance.tournament_id,
                    "model": "Round",
                    "id": instance.pk,
                    "data": data,
                },
            )
        )


@receiver([post_save, pre_delete], sender=Heat)
def heat_update(sender, instance, created=None, **kwargs):
    action = "create" if created else "update" if created is False else "delete"
    serializer = HeatSerializer(instance=instance)
    data = serializer.data

    transaction.on_commit(
        lambda: group_send(
            "tournament",
            {
                "type": "update",
                "action": action,
                "tournament_id": instance.round.tournament_id,
                "model": "Heat",
                "id": instance.pk,
                "data": data,
            },
        )
    )


@receiver(m2m_changed, sender=Heat.couples.through)
def update_heat_couple(sender, action, instance, pk_set, **kwargs):
    if action in ["post_add", "post_remove"] and pk_set:
        serializer = HeatSerializer(instance=instance)
        data = serializer.data

        transaction.on_commit(
            lambda: group_send(
                "tournament",
                {
                    "type": "update",
                    "action": "update",
                    "tournament_id": instance.round.tournament_id,
                    "model": "Heat",
                    "id": instance.pk,
                    "data": data,
                },
            )
        )


@receiver([post_save, pre_delete], sender=Adjudicator)
def adjudicator_update(sender, instance, created=None, **kwargs):
    action = "create" if created else "update" if created is False else "delete"
    serializer = AdjudicatorSerializer(instance=instance)
    data = serializer.data

    transaction.on_commit(
        lambda: group_send(
            "tournament",
            {
                "type": "update",
                "action": action,
                "tournament_id": instance.tournament_id,
                "model": "Adjudicator",
                "id": instance.pk,
                "data": data,
            },
        )
    )


@receiver([post_save, pre_delete], sender=Couple)
def couple_update(sender, instance, created=None, **kwargs):
    action = "create" if created else "update" if created is False else "delete"
    serializer = CoupleSerializer(instance=instance)
    data = serializer.data

    transaction.on_commit(
        lambda: group_send(
            "tournament",
            {
                "type": "update",
                "action": action,
                "tournament_id": instance.tournament_id,
                "model": "Couple",
                "id": instance.pk,
                "data": data,
            },
        )
    )


@receiver([post_save, pre_delete], sender=Mark)
def mark_update(sender, instance, created=None, **kwargs):
    action = "create" if created else "update" if created is False else "delete"
    serializer = MarkSerializer(instance=instance)
    data = serializer.data

    transaction.on_commit(
        lambda: group_send(
            "tournament",
            {
                "type": "update",
                "action": action,
                "tournament_id": instance.adjudicator.tournament_id,
                "model": "Mark",
                "id": instance.pk,
                "data": data,
            },
        )
    )
