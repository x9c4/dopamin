from backend.models import Adjudicator, Couple, Tournament

tournament, _ = Tournament.objects.get_or_create(pk=1)

Adjudicator.objects.get_or_create(tournament=tournament, slug="ADJ_1", defaults={"name": "Ajd 1"})

for i in range(100):
    Couple.objects.get_or_create(tournament=tournament, number=str(i))
