import * as React from 'react';
import App from '@app/index';
import { ReconWebSocket } from '@app/utils/ReconWebSocket';
import { createRoot, Root } from 'react-dom/client';
import { act } from 'react-dom/test-utils';
import pretty from 'pretty';
import fetchMock from 'jest-fetch-mock';

jest.mock('@app/utils/ReconWebSocket');

beforeAll(() => {
  fetchMock.mockResponse('{"results": []}');
  const mockedReconWebSocket: jest.Mock<typeof ReconWebSocket> = ReconWebSocket as unknown as jest.Mock<
    typeof ReconWebSocket
  >;
  mockedReconWebSocket.prototype.connect.mockImplementation(() => undefined);
});

let container: Element | null = null;
let root: Root | null = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement('div');
  document.body.appendChild(container);
  root = createRoot(container);
});

afterEach(() => {
  // cleanup on exiting
  act(() => {
    (root as Root).unmount();
  });
  (container as Element).remove();
  container = null;
});

describe('App tests', () => {
  it('should render default App component', async () => {
    await act(async () => {
      root.render(<App />);
    });
    expect(pretty(container?.innerHTML)).toMatchSnapshot();
  });

  it('should render a nav-toggle button', async () => {
    await act(async () => {
      root.render(<App />);
    });
    const button = container?.querySelector('#nav-toggle');
    expect(button).not.toBeNull();
  });

  it('should hide the sidebar on smaller viewports', async () => {
    Object.defineProperty(window, 'innerWidth', { writable: true, configurable: true, value: 600 });
    await act(async () => {
      root.render(<App />);
      window.dispatchEvent(new Event('resize'));
    });
    expect(container?.querySelector('#page-sidebar')?.classList).toContain('pf-m-collapsed');
  });

  it('should expand the sidebar on larger viewports', async () => {
    Object.defineProperty(window, 'innerWidth', { writable: true, configurable: true, value: 1200 });
    await act(async () => {
      root.render(<App />);
      window.dispatchEvent(new Event('resize'));
    });
    expect(container?.querySelector('#page-sidebar')?.classList).toContain('pf-m-expanded');
  });

  it('should hide the sidebar when clicking the nav-toggle button', async () => {
    Object.defineProperty(window, 'innerWidth', { writable: true, configurable: true, value: 1200 });
    await act(async () => {
      root.render(<App />);
      window.dispatchEvent(new Event('resize'));
    });
    const button = container?.querySelector('#nav-toggle');
    expect(container?.querySelector('#page-sidebar')?.classList).toContain('pf-m-expanded');
    act(() => {
      button?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });
    expect(container?.querySelector('#page-sidebar')?.classList).toContain('pf-m-collapsed');
    expect(container?.querySelector('#page-sidebar')?.classList).not.toContain('pf-m-expanded');
  });
});
