import * as React from 'react';
import { PageSection, Title } from '@patternfly/react-core';
import { useAuth } from '@app/api/auth';

const Dashboard: React.FunctionComponent = () => {
  const { isAuthenticated, username } = useAuth();

  return (
    <PageSection>
      <Title headingLevel="h1" size="lg">
        {isAuthenticated ? `Welcome ${username}` : 'Dopamin'}
      </Title>
    </PageSection>
  );
};

export { Dashboard };
