import React from 'react';
import { useSubmit } from 'react-router-dom';
import { LoginPage, LoginForm } from '@patternfly/react-core';

const Login: React.FunctionComponent = () => {
  const submit = useSubmit();
  const [username, setUsername] = React.useState('');
  const [isValidUsername, setIsValidUsername] = React.useState(true);
  const [password, setPassword] = React.useState('');
  const [isValidPassword, setIsValidPassword] = React.useState(true);

  const handleUsernameChange = (_event: React.FormEvent<HTMLInputElement>, value: string) => {
    setUsername(value);
  };

  const handlePasswordChange = (_event: React.FormEvent<HTMLInputElement>, value: string) => {
    setPassword(value);
  };

  const onLoginSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    setIsValidUsername(!!username);
    setIsValidPassword(!!password);
    // Until we can tell patternfly to use react router Form...
    submit({ username, password }, { method: 'post' });
  };

  return (
    <LoginPage loginTitle="Dopamin Login">
      <LoginForm
        method="post"
        action="/login"
        onSubmit={onLoginSubmit}
        usernameValue={username}
        onChangeUsername={handleUsernameChange}
        isValidUsername={isValidUsername}
        passwordValue={password}
        onChangePassword={handlePasswordChange}
        isValidPassword={isValidPassword}
      />
    </LoginPage>
  );
};

export { Login };
