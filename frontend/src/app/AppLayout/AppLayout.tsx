import * as React from 'react';
import { useFetcher, NavLink, useLocation, useNavigate, Outlet } from 'react-router-dom';
import {
  Avatar,
  Brand,
  Button,
  ButtonVariant,
  Divider,
  Dropdown,
  DropdownGroup,
  DropdownItem,
  DropdownList,
  Masthead,
  MastheadBrand,
  MastheadContent,
  MastheadMain,
  MastheadToggle,
  MenuToggle,
  MenuToggleElement,
  Nav,
  NavExpandable,
  NavItem,
  NavList,
  Page,
  PageSidebar,
  PageSidebarBody,
  PageToggleButton,
  SkipToContent,
  Toolbar,
  ToolbarContent,
  ToolbarGroup,
  ToolbarItem,
} from '@patternfly/react-core';
import BarsIcon from '@patternfly/react-icons/dist/esm/icons/bars-icon';
import BellIcon from '@patternfly/react-icons/dist/esm/icons/bell-icon';
import CogIcon from '@patternfly/react-icons/dist/esm/icons/cog-icon';
import HelpIcon from '@patternfly/react-icons/dist/esm/icons/help-icon';
import QuestionCircleIcon from '@patternfly/react-icons/dist/esm/icons/question-circle-icon';
import EllipsisVIcon from '@patternfly/react-icons/dist/esm/icons/ellipsis-v-icon';
import { routes, AppRouteConfig } from '@app/routes';
import logo from '@app/bgimages/Dopamin-Logo.svg';
import mobileLogo from '@app/bgimages/Dopamin-Logo-Mobile.svg';
import { NotificationContext } from '@app/NotificationContext/NotificationContext';
import imgAvatar from '@app/bgimages/img_avatar.svg';
import { useAuth } from '@app/api/auth';

interface IAppLayout {}

const AppLayout: React.FunctionComponent<IAppLayout> = () => {
  const location = useLocation();
  const { isAuthenticated, username, user_id } = useAuth();
  const navigate = useNavigate();
  const logoutFetcher = useFetcher();

  const [isDropdownOpen, setIsDropdownOpen] = React.useState(false);
  const [isKebabDropdownOpen, setIsKebabDropdownOpen] = React.useState(false);
  const [isFullKebabDropdownOpen, setIsFullKebabDropdownOpen] = React.useState(false);

  const onDropdownToggle = () => {
    setIsDropdownOpen(!isDropdownOpen);
  };

  const onDropdownSelect = () => {
    setIsDropdownOpen(false);
  };

  const onKebabDropdownToggle = () => {
    setIsKebabDropdownOpen(!isKebabDropdownOpen);
  };

  const onKebabDropdownSelect = () => {
    setIsKebabDropdownOpen(false);
  };

  const onFullKebabDropdownToggle = () => {
    setIsFullKebabDropdownOpen(!isFullKebabDropdownOpen);
  };

  const onFullKebabDropdownSelect = () => {
    setIsFullKebabDropdownOpen(false);
  };

  const kebabDropdownItems = (
    <>
      <DropdownItem key="settings">
        <CogIcon /> Settings
      </DropdownItem>
      <DropdownItem key="help">
        <HelpIcon /> Help
      </DropdownItem>
    </>
  );
  const userDropdownItems = isAuthenticated ? (
    <>
      <DropdownItem key="profile" onClick={() => navigate(`/user/${user_id}`)}>
        My profile
      </DropdownItem>
      <DropdownItem key="user" onClick={() => navigate('/user')}>
        User management
      </DropdownItem>
      <DropdownItem key="logout" onClick={() => logoutFetcher.submit(null, { method: 'post', action: '/logout' })}>
        Logout
      </DropdownItem>
    </>
  ) : (
    <DropdownItem key="user" onClick={() => navigate('/login')}>
      Login
    </DropdownItem>
  );

  const headerToolbar = (
    <Toolbar id="toolbar" isFullHeight isStatic>
      <ToolbarContent>
        <ToolbarGroup
          variant="icon-button-group"
          align={{ default: 'alignRight' }}
          spacer={{ default: 'spacerNone', md: 'spacerMd' }}
        >
          <ToolbarItem>
            <Button aria-label="Notifications" variant={ButtonVariant.plain} icon={<BellIcon />} />
          </ToolbarItem>
          <ToolbarGroup variant="icon-button-group" visibility={{ default: 'hidden', lg: 'visible' }}>
            <ToolbarItem>
              <Button aria-label="Settings" variant={ButtonVariant.plain} icon={<CogIcon />} />
            </ToolbarItem>
            <ToolbarItem>
              <Button aria-label="Help" variant={ButtonVariant.plain} icon={<QuestionCircleIcon />} />
            </ToolbarItem>
          </ToolbarGroup>
          <ToolbarItem visibility={{ default: 'hidden', md: 'visible', lg: 'hidden' }}>
            <Dropdown
              isOpen={isKebabDropdownOpen}
              onSelect={onKebabDropdownSelect}
              onOpenChange={(isOpen: boolean) => setIsKebabDropdownOpen(isOpen)}
              popperProps={{ position: 'right' }}
              toggle={(toggleRef: React.Ref<MenuToggleElement>) => (
                <MenuToggle
                  ref={toggleRef}
                  onClick={onKebabDropdownToggle}
                  isExpanded={isKebabDropdownOpen}
                  variant="plain"
                  aria-label="Settings and help"
                >
                  <EllipsisVIcon aria-hidden="true" />
                </MenuToggle>
              )}
            >
              <DropdownList>{kebabDropdownItems}</DropdownList>
            </Dropdown>
          </ToolbarItem>
          <ToolbarItem visibility={{ md: 'hidden' }}>
            <Dropdown
              isOpen={isFullKebabDropdownOpen}
              onSelect={onFullKebabDropdownSelect}
              onOpenChange={(isOpen: boolean) => setIsFullKebabDropdownOpen(isOpen)}
              popperProps={{ position: 'right' }}
              toggle={(toggleRef: React.Ref<MenuToggleElement>) => (
                <MenuToggle
                  ref={toggleRef}
                  onClick={onFullKebabDropdownToggle}
                  isExpanded={isFullKebabDropdownOpen}
                  variant="plain"
                  aria-label="Toolbar menu"
                >
                  <EllipsisVIcon aria-hidden="true" />
                </MenuToggle>
              )}
            >
              <DropdownGroup key="group 2" label="User actions">
                <DropdownList>{userDropdownItems}</DropdownList>
              </DropdownGroup>
              <Divider />
              <DropdownList>{kebabDropdownItems}</DropdownList>
            </Dropdown>
          </ToolbarItem>
        </ToolbarGroup>
        <ToolbarItem visibility={{ default: 'hidden', md: 'visible' }}>
          <Dropdown
            isOpen={isDropdownOpen}
            onSelect={onDropdownSelect}
            onOpenChange={(isOpen: boolean) => setIsDropdownOpen(isOpen)}
            popperProps={{ position: 'right' }}
            toggle={(toggleRef: React.Ref<MenuToggleElement>) => (
              <MenuToggle
                ref={toggleRef}
                onClick={onDropdownToggle}
                isFullHeight
                isExpanded={isDropdownOpen}
                icon={<Avatar src={imgAvatar} alt="" />}
              >
                {username}
              </MenuToggle>
            )}
          >
            <DropdownList>{userDropdownItems}</DropdownList>
          </Dropdown>
        </ToolbarItem>
      </ToolbarContent>
    </Toolbar>
  );

  const Header = (
    <Masthead>
      <MastheadToggle>
        <PageToggleButton variant="plain">
          <BarsIcon />
        </PageToggleButton>
      </MastheadToggle>
      <MastheadMain>
        <MastheadBrand>
          <Brand src={logo} alt="Dopamin Logo" onClick={() => navigate('/')}>
            <source media="(min-width: 1200px)" srcSet={logo} />
            <source srcSet={mobileLogo} />
          </Brand>
        </MastheadBrand>
      </MastheadMain>
      <MastheadContent>
        <div id="header-portal" />
        {headerToolbar}
      </MastheadContent>
    </Masthead>
  );

  const renderNavItem = (route: AppRouteConfig, path: string, index: number) => (
    <NavItem
      key={index}
      id={`${route.label}-${index}`}
      isActive={route.exact ? location.pathname === path : location.pathname.startsWith(path)}
    >
      <NavLink end={route.exact} to={path}>
        {route.label}
      </NavLink>
    </NavItem>
  );

  const renderNavGroup = (route: AppRouteConfig, path: string, children: React.ReactNode[], index: number) => (
    <NavExpandable
      key={index}
      id={`${route.label}-${index}`}
      title={route.label as string}
      isActive={route.exact ? location.pathname === path : location.pathname.startsWith(path)}
    >
      {children}
    </NavExpandable>
  );

  const renderNavRoute = (route: AppRouteConfig, basePath: string, index: number) => {
    const path = basePath + (route?.path || '');
    const children = route.children
      ?.map((childRoute, childIndex) => renderNavRoute(childRoute, path, childIndex))
      .filter((item) => !!item);
    if (children?.length) {
      if (route.label) {
        return renderNavGroup(route, path, children, index);
      }
      return children;
    }
    return route.label && (route.path || route.index) && renderNavItem(route, path, index);
  };

  const Navigation = (
    <Nav id="nav-primary-simple">
      <NavList id="nav-list-simple">{routes.map((route, idx) => renderNavRoute(route, '', idx))}</NavList>
    </Nav>
  );

  const Sidebar = (
    <PageSidebar>
      <PageSidebarBody>{Navigation}</PageSidebarBody>
    </PageSidebar>
  );

  const pageId = 'primary-app-container';

  const PageSkipToContent = <SkipToContent href={`#${pageId}`}>Skip to Content</SkipToContent>;
  // TODO: breadcrumbs
  return (
    <Page mainContainerId={pageId} header={Header} sidebar={Sidebar} isManagedSidebar skipToContent={PageSkipToContent}>
      {isAuthenticated && <NotificationContext />}
      <Outlet />
    </Page>
  );
};

export { AppLayout };
