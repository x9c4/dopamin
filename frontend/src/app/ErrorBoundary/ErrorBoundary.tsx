import * as React from 'react';
import { ExclamationTriangleIcon } from '@patternfly/react-icons';
import { PageSection, Title, Button, EmptyState, EmptyStateIcon, EmptyStateBody } from '@patternfly/react-core';
import { useNavigate, useRouteError, isRouteErrorResponse } from 'react-router-dom';

const ErrorBoundary: React.FunctionComponent = () => {
  const error = useRouteError();
  let errorTitle: string = 'Error';
  let errorMessage: string;

  if (isRouteErrorResponse(error)) {
    errorTitle = error.status.toString();
    errorMessage = error.statusText;
  } else if (error instanceof Error) {
    errorMessage = error.message;
  } else if (typeof error === 'string') {
    errorMessage = error;
  } else {
    console.log(error);
    errorMessage = 'Unknown Error';
  }

  function GoHomeBtn() {
    const navigate = useNavigate();
    function handleClick() {
      navigate('/');
    }
    return <Button onClick={handleClick}>Take me home</Button>;
  }

  return (
    <PageSection>
      <EmptyState variant="full">
        <EmptyStateIcon icon={ExclamationTriangleIcon} />
        <Title headingLevel="h1" size="lg">
          {errorTitle}
        </Title>
        <EmptyStateBody>{errorMessage}</EmptyStateBody>
        <GoHomeBtn />
      </EmptyState>
    </PageSection>
  );
};

export { ErrorBoundary };
