import * as React from 'react';
import { Form, useActionData } from 'react-router-dom';
import { Alert, Button, ButtonType, Form as PfForm, FormAlert, Modal, ModalVariant } from '@patternfly/react-core';
import { FormTextInput, FormCheckboxInput } from '@app/utils/FormInput';

export const NewUserModal: React.FunctionComponent = () => {
  const actionData = useActionData();
  const [isModalOpen, setModalOpen] = React.useState(false);

  const nonFieldErrors = actionData && actionData['non_field_errors'];

  const handleModalToggle = () => {
    setModalOpen(!isModalOpen);
  };
  return (
    <>
      <Button variant="primary" onClick={handleModalToggle}>
        New User
      </Button>
      <Form noValidate id="new-user-form" method="post" />
      <Modal
        variant={ModalVariant.small}
        title="Create new user"
        isOpen={isModalOpen}
        onClose={handleModalToggle}
        actions={[
          <Button key="create" variant="primary" form="new-user-form" type={ButtonType.submit}>
            Create
          </Button>,
          <Button key="cancel" variant="link" onClick={handleModalToggle}>
            Cancel
          </Button>,
        ]}
      >
        <PfForm id="new-user-form-layout">
          {nonFieldErrors && (
            <FormAlert>
              <Alert variant="danger" title={nonFieldErrors.join('; ')} aria-live="polite" isInline />
            </FormAlert>
          )}
          <FormTextInput name="username" label="Username" isRequired form="new-user-form" />
          <FormTextInput name="password" label="Password" isRequired type="password" form="new-user-form" />
          <FormCheckboxInput label="is_staff" form="new-user-form" name="is_staff" />
          <FormTextInput name="firstname" label="First Name" form="new-user-form" />
          <FormTextInput name="lastname" label="Last Name" form="new-user-form" />
          <FormTextInput name="email" label="E-mail" type="email" form="new-user-form" />
        </PfForm>
      </Modal>
    </>
  );
};
