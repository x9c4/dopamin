import * as React from 'react';
import { Link, Form, useLoaderData, useSearchParams } from 'react-router-dom';
import {
  Button,
  ButtonType,
  DescriptionList,
  DescriptionListDescription,
  DescriptionListGroup,
  DescriptionListTerm,
  PageSection,
  Pagination,
  Stack,
  StackItem,
  Title,
  Toolbar,
  ToolbarContent,
  ToolbarItem,
} from '@patternfly/react-core';
import { Table, Caption, Thead, Tr, Th, Tbody, Td } from '@patternfly/react-table';
import CheckIcon from '@patternfly/react-icons/dist/esm/icons/check-icon';
import TimesIcon from '@patternfly/react-icons/dist/esm/icons/times-icon';
import SyncIcon from '@patternfly/react-icons/dist/esm/icons/sync-icon';
import { InlineEdit } from '@app/utils/InlineEdit';
import { useDocumentTitle } from '@app/utils/useDocumentTitle';
import { NewUserModal } from '@app/User/NewUser';

interface IUser {
  id: number;
  href: string;
  username: string;
  password?: string;
  is_staff: boolean;
  first_name: string;
  last_name: string;
  email: string;
}

interface IUsers {
  count: number;
  results: IUser[];
}

export const UserIndex: React.FunctionComponent = () => {
  useDocumentTitle('Dopamin | Users');
  const [searchParams, setSearchParams] = useSearchParams();
  const loaderData = useLoaderData() as IUsers;
  const users = loaderData.results;

  const onSetPage = (_event: React.MouseEvent | React.KeyboardEvent | MouseEvent, newPage: number) => {
    searchParams.set('page', newPage.toString());
    setSearchParams(searchParams, { replace: true });
  };

  const onPerPageSelect = (
    _event: React.MouseEvent | React.KeyboardEvent | MouseEvent,
    newPerPage: number,
    newPage: number,
  ) => {
    searchParams.set('per_page', newPerPage.toString());
    searchParams.set('page', newPage.toString());
    setSearchParams(searchParams, { replace: true });
  };

  return (
    <>
      <PageSection>
        <Title headingLevel="h1" size="lg">
          User Index
        </Title>
        <Toolbar>
          <ToolbarContent>
            <ToolbarItem>
              <NewUserModal />
            </ToolbarItem>
          </ToolbarContent>
        </Toolbar>
      </PageSection>
      <PageSection>
        <Pagination
          itemCount={loaderData.count}
          page={Number(searchParams.get('page')) || 1}
          perPage={Number(searchParams.get('per_page')) || 20}
          onSetPage={onSetPage}
          onPerPageSelect={onPerPageSelect}
        >
          <Table>
            <Caption>Users</Caption>
            <Thead>
              <Tr>
                <Th>Username</Th>
                <Th>Staff</Th>
                <Th>First Name</Th>
                <Th>Last Name</Th>
                <Th>E-mail</Th>
              </Tr>
            </Thead>
            <Tbody>
              {users.map(({ id, username, is_staff, first_name, last_name, email }) => (
                <Tr key={id}>
                  <Td>
                    <Link to={id.toString()}>{username}</Link>
                  </Td>
                  <Td>{is_staff && 'yes'}</Td>
                  <Td>{first_name}</Td>
                  <Td>{last_name}</Td>
                  <Td>{email}</Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        </Pagination>
      </PageSection>
    </>
  );
};

export const User: React.FunctionComponent = () => {
  useDocumentTitle('Dopamin | Users');
  const loaderData = useLoaderData();
  const { username, is_staff, first_name, last_name, email } = loaderData as IUser;

  return (
    <PageSection>
      <Stack hasGutter>
        <StackItem>
          <Title headingLevel="h1">User {username}</Title>
        </StackItem>
        <StackItem isFilled>
          <DescriptionList>
            <DescriptionListGroup>
              <DescriptionListTerm>Password</DescriptionListTerm>
              <DescriptionListDescription>
                <InlineEdit type="password" attribute="password" value="" altvalue="********" />
              </DescriptionListDescription>
            </DescriptionListGroup>
            <DescriptionListGroup>
              <DescriptionListTerm>First Name</DescriptionListTerm>
              <DescriptionListDescription>
                <InlineEdit attribute="first_name" value={first_name} />
              </DescriptionListDescription>
            </DescriptionListGroup>
            <DescriptionListGroup>
              <DescriptionListTerm>Last Name</DescriptionListTerm>
              <DescriptionListDescription>
                <InlineEdit attribute="last_name" value={last_name} />
              </DescriptionListDescription>
            </DescriptionListGroup>
            <DescriptionListGroup>
              <DescriptionListTerm>E-mail</DescriptionListTerm>
              <DescriptionListDescription>
                <InlineEdit attribute="email" value={email} />
              </DescriptionListDescription>
            </DescriptionListGroup>
            <DescriptionListGroup>
              <DescriptionListTerm>Is Staff</DescriptionListTerm>
              <DescriptionListDescription>
                {is_staff ? <CheckIcon /> : <TimesIcon />}
                <Button
                  variant="plain"
                  id="user-is-staff"
                  form="user-is-staff-form"
                  name="is_staff"
                  value={(!is_staff).toString()}
                  type={ButtonType.submit}
                >
                  <SyncIcon />
                </Button>
                <Form noValidate id="user-is-staff-form" method="patch" />
              </DescriptionListDescription>
            </DescriptionListGroup>
          </DescriptionList>
        </StackItem>
        <StackItem>
          <Toolbar>
            <ToolbarContent>
              <ToolbarItem>
                <Form noValidate id="user-actions-form" method="delete">
                  <Button key="delete" variant="danger" type={ButtonType.submit}>
                    Delete
                  </Button>
                </Form>
              </ToolbarItem>
            </ToolbarContent>
          </Toolbar>
        </StackItem>
      </Stack>
    </PageSection>
  );
};
