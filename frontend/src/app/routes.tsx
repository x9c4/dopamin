import * as React from 'react';
import { RouteObject } from 'react-router-dom';
import { AppLayout } from '@app/AppLayout/AppLayout';
import { ErrorBoundary } from '@app/ErrorBoundary/ErrorBoundary';
import { Login } from '@app/Login/Login';
import { Dashboard } from '@app/Dashboard/Dashboard';
import { TournamentIndex } from '@app/Tournament/TournamentIndex';
import { TournamentBoard } from '@app/Tournament/Tournament';
import { AdjudicatorBoard } from '@app/MarkSheet/MarkSheet';
import { User, UserIndex } from '@app/User/User';
import { userLoader, usersLoader, userCreateAction, userAction } from '@app/api/user';
import {
  tournamentLoader,
  tournamentsLoader,
  tournamentCreateAction,
  tournamentAction,
  tournamentStateAction,
} from '@app/api/tournament';
import {
  adjudicatorLoader,
  adjudicatorsLoader,
  adjudicatorCreateAction,
  adjudicatorAction,
} from '@app/api/adjudicator';
import { coupleLoader, couplesLoader, coupleCreateAction, coupleAction } from '@app/api/couple';
import { rootLoader, loginLoader, loginAction, logoutAction } from '@app/api/auth';

export interface IAppRoute {
  label?: string; // Excluding the label will exclude the route from the nav sidebar in AppLayout
  exact?: boolean;
  title?: string;
  children?: AppRouteConfig[];
}

export type AppRouteConfig = RouteObject & IAppRoute;

const routes: AppRouteConfig[] = [
  {
    id: 'root',
    element: <AppLayout />,
    loader: rootLoader,
    path: '/',
    children: [
      {
        errorElement: <ErrorBoundary />,
        children: [
          {
            path: 'login',
            element: <Login />,
            loader: loginLoader,
            action: loginAction,
          },
          {
            path: 'logout',
            action: logoutAction,
          },
          {
            element: <Dashboard />,
            index: true,
            exact: true,
            label: 'Dashboard',
            title: 'Dopamin | Main Dashboard',
          },
          {
            path: 'tournament',
            label: 'Tournaments',
            title: 'Dopamin | Tournaments',
            children: [
              {
                element: <TournamentIndex />,
                index: true,
                loader: tournamentsLoader,
                action: tournamentCreateAction,
              },
              {
                element: <TournamentBoard />,
                exact: true,
                // label: 'Tournament',
                path: ':tournament_id',
                title: 'Dopamin | Tournament Dashboard',
                loader: tournamentLoader,
                action: tournamentAction,
                children: [
                  {
                    path: 'start',
                    action: ({ request, params }) => tournamentStateAction('start', { request, params }),
                  },
                  {
                    path: 'adjudicator',
                    children: [
                      {
                        index: true,
                        loader: adjudicatorsLoader,
                        action: adjudicatorCreateAction,
                      },
                      {
                        element: <AdjudicatorBoard />,
                        exact: true,
                        path: ':adjudicator_id',
                        loader: adjudicatorLoader,
                        action: adjudicatorAction,
                        title: 'Dopamin | Tournament Dashboard',
                      },
                    ],
                  },
                  {
                    path: 'couple',
                    children: [
                      {
                        index: true,
                        loader: couplesLoader,
                        action: coupleCreateAction,
                      },
                      {
                        path: ':couple_id',
                        loader: coupleLoader,
                        action: coupleAction,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            exact: false,
            path: 'user/',
            title: 'Dopamin | Users',
            label: 'Users',
            children: [
              {
                element: <UserIndex />,
                index: true,
                loader: usersLoader,
                action: userCreateAction,
              },
              {
                element: <User />,
                path: ':user_id',
                loader: userLoader,
                action: userAction,
              },
            ],
          },
        ],
      },
    ],
  },
];

export { routes };
