import { redirect } from 'react-router-dom';
import { get_cookie } from '@app/api/get_cookie';
import { AuthProvider } from '@app/api/auth';

export const userLoader = async ({ request, params }) => {
  await AuthProvider.wait(true);
  const response = await fetch(`/api/v1/user/${params.user_id}/`, {
    credentials: 'same-origin',
    signal: request.signal,
  });
  if (response.status == 404) return redirect('../?index');
  return response;
};

export const usersLoader = async ({ request }) => {
  await AuthProvider.wait(true);
  const searchParams = new URL(request.url).searchParams;
  const per_page = Number(searchParams.get('per_page')) || 20;
  const page = Number(searchParams.get('page')) || 1;

  searchParams.delete('index');
  searchParams.delete('page');
  searchParams.delete('per_page');
  searchParams.set('limit', per_page.toString());
  searchParams.set('offset', ((page - 1) * per_page).toString());

  return fetch(`/api/v1/user/?${searchParams}`, {
    credentials: 'same-origin',
    signal: request.signal,
  });
};

export const userCreateAction = async ({ request }) => {
  const response = await fetch('/api/v1/user/', {
    method: request.method,
    body: JSON.stringify(Object.fromEntries(await request.formData())),
    headers: { 'Content-Type': 'application/json', 'X-CSRFToken': get_cookie('csrftoken') },
    credentials: 'same-origin',
    signal: request.signal,
  });
  if (response.status == 400) return response;
  if (response.status == 401) return redirect('/login');
  if (!response.ok) throw Error('Error!');
  return null;
};

export const userAction = async ({ request, params }) => {
  const response = await fetch(`/api/v1/user/${params.user_id}/`, {
    method: request.method,
    body: request.method == 'PATCH' ? JSON.stringify(Object.fromEntries(await request.formData())) : null,
    headers: { 'Content-Type': 'application/json', 'X-CSRFToken': get_cookie('csrftoken') },
    credentials: 'same-origin',
    signal: request.signal,
  });
  if (response.status == 400) return response;
  if (response.status == 401) return redirect('/login');
  if (!response.ok) throw Error('Error!');
  if (request.method.toUpperCase() == 'DELETE') return redirect('../?index');
  return null;
};
