import { redirect, useRouteLoaderData, LoaderFunctionArgs } from 'react-router-dom';
import { get_cookie } from '@app/api/get_cookie';

interface IAuthProvider {
  isAuthenticated: boolean | null;
  user_id: number | null;
  username: string | null;
  wait: (need_login?: boolean) => Promise<IAuthProvider>;
  _promise: Promise<IAuthProvider> | null;
}

const init_login = async () => {
  const response = await fetch('/api/v1/login/', {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
    credentials: 'same-origin',
  });
  if (response.ok) {
    const data = await response.json();
    if (data.id != null) {
      AuthProvider.isAuthenticated = true;
      AuthProvider.user_id = data.id;
      AuthProvider.username = data.username;
    } else {
      AuthProvider.isAuthenticated = false;
      AuthProvider.user_id = null;
      AuthProvider.username = null;
    }
    return AuthProvider;
  }
  throw Error('Error while connecting to api');
};

const login = async (request: Request) => {
  const response = await fetch('/api/v1/login/', {
    method: 'POST',
    body: JSON.stringify(Object.fromEntries(await request.formData())),
    headers: { 'Content-Type': 'application/json', 'X-CSRFToken': get_cookie('csrftoken') },
    credentials: 'same-origin',
  });
  if (response.ok) {
    const data = await response.json();
    if (data.id != null) {
      AuthProvider.isAuthenticated = true;
      AuthProvider.user_id = data.id;
      AuthProvider.username = data.username;
    } else {
      AuthProvider.isAuthenticated = false;
      AuthProvider.user_id = null;
      AuthProvider.username = null;
    }
    return AuthProvider;
  }
  // TODO: Find a way for validation errors.
  if (response.status == 400) return AuthProvider;
  throw Error('Error during login');
};

const logout = async () => {
  const response = await fetch('/api/v1/login/', {
    method: 'DELETE',
    headers: { 'X-CSRFToken': get_cookie('csrftoken') },
    credentials: 'same-origin',
  });
  if (response.ok) {
    AuthProvider.isAuthenticated = false;
    AuthProvider.user_id = null;
    AuthProvider.username = null;
    return AuthProvider;
  }
  throw Error('Error during logout');
};

export const AuthProvider: IAuthProvider = {
  isAuthenticated: null,
  user_id: null,
  username: null,
  wait: async function (need_login = false) {
    if (AuthProvider._promise === null) {
      AuthProvider._promise = init_login();
    }
    if (need_login) {
      await AuthProvider._promise;
      if (!AuthProvider.isAuthenticated) throw redirect('/login');
    }
    return AuthProvider._promise;
  },
  _promise: null,
};

export const useAuth = () => useRouteLoaderData('root') as IAuthProvider;

export async function rootLoader() {
  return await AuthProvider.wait();
}

export async function loginLoader() {
  if ((await AuthProvider.wait()).isAuthenticated) return redirect('/');
  return null;
}

export async function loginAction({ request }: LoaderFunctionArgs) {
  await AuthProvider.wait();
  AuthProvider._promise = login(request);
  if ((await AuthProvider._promise).isAuthenticated) return redirect('/');
  return null;
}

export async function logoutAction() {
  if ((await AuthProvider.wait()).isAuthenticated) {
    AuthProvider._promise = logout();
    await AuthProvider._promise;
  }
  return redirect('/login');
}
