import { redirect } from 'react-router-dom';
import { get_cookie } from '@app/api/get_cookie';
import { AuthProvider } from '@app/api/auth';

export const adjudicatorLoader = async ({ request, params }) => {
  await AuthProvider.wait(true);
  const response = await fetch(`/api/v1/tournament/${params.tournament_id}/adjudicator/${params.adjudicator_id}/`, {
    credentials: 'same-origin',
    signal: request.signal,
  });
  if (response.status == 404) return redirect('../?index');
  return response;
};

export const adjudicatorsLoader = async ({ request, params }) => {
  await AuthProvider.wait(true);
  const url = new URL(request.url);
  const per_page = Number(url.searchParams.get('per_page')) || 20;
  const page = Number(url.searchParams.get('page')) || 1;

  return fetch(
    `/api/v1/tournament/${params.tournament_id}/adjudicator/?limit=${per_page}&offset=${(page - 1) * per_page}`,
    {
      credentials: 'same-origin',
      signal: request.signal,
    },
  );
};

export const adjudicatorCreateAction = async ({ request, params }) => {
  const response = await fetch(`/api/v1/tournament/${params.tournament_id}/adjudicator/`, {
    method: request.method,
    body: JSON.stringify(Object.fromEntries(await request.formData())),
    headers: { 'Content-Type': 'application/json', 'X-CSRFToken': get_cookie('csrftoken') },
    credentials: 'same-origin',
    signal: request.signal,
  });
  if (response.status == 400) return response;
  if (response.status == 401) return redirect('/login');
  if (!response.ok) throw Error('Error!');
  return null;
};

export const adjudicatorAction = async ({ request, params }) => {
  const response = await fetch(`/api/v1/tournament/${params.tournament_id}/adjudicator/${params.adjudicator_id}/`, {
    method: request.method,
    body: request.method == 'PATCH' ? JSON.stringify(Object.fromEntries(await request.formData())) : null,
    headers: { 'Content-Type': 'application/json', 'X-CSRFToken': get_cookie('csrftoken') },
    credentials: 'same-origin',
    signal: request.signal,
  });
  if (response.status == 400) return response;
  if (response.status == 401) return redirect('/login');
  if (!response.ok) throw Error('Error!');
  if (request.method.toUpperCase() == 'DELETE') return redirect('../?index');
  return null;
};
