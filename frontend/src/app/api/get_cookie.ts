export function get_cookie(name: string) {
  for (const item of document.cookie.split(';')) {
    const [key, value] = item.split('=');
    if (key == name) {
      return value;
    }
  }
  return '';
}
