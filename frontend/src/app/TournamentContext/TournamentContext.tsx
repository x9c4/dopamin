import * as React from 'react';
import { ReconWebSocket } from '@app/utils/ReconWebSocket';
import { useAuth } from '@app/api/auth';
import { get_cookie } from '@app/api/get_cookie';

interface ITournament {
  id: number;
  href: string;
  name: string;
  description: string | null;
  date: string | null;
  state: string;
  rounds_href: string;
  heats_href: string;
  adjudicators_href: string;
  couples_href: string;
  marks_href: string;
}

interface IRound {
  id: number;
  href: string;
  slug: string;
  dances: string[];
  marks_target?: number;
  round_type: string;
  state: string;
  heats_href: string;
  heats: string[];
}

interface IHeat {
  id: number;
  href: string;
  dance: string;
  number: number;
  round: string;
  couples: string[];
}

interface IAdjudicator {
  id: number;
  href: string;
  slug: string;
  name: string;
  user?: string;
  username?: string;
}

interface ICouple {
  id: number;
  href: string;
  number: string;
  lead: string;
  follow: string;
}

interface IMark {
  id: number;
  href: string;
  value: number;
  remark: number;
  adjudicator: string;
  couple: string;
  heat: string;
}

interface ITournamentState {
  base_url: string;
  tournament: ITournament;
  round_hrefs: string[];
  rounds: { [Key: string]: IRound };
  heat_hrefs: string[];
  heats: { [Key: string]: IHeat };
  adjudicator_hrefs: string[];
  adjudicators: { [Key: string]: IAdjudicator };
  couple_hrefs: string[];
  couples: { [Key: string]: ICouple };
  mark_hrefs: string[];
  marks: { [Key: string]: IMark };
}

interface ITournamentContext {
  tournamentState: ITournamentState;
  dispatch: React.Dispatch<IAction>;
}

interface ITournamentContextProvider {
  id: number;
  children: React.ReactElement;
}

type ModelIndex = {
  Tournament: ITournament;
  Round: IRound;
  Heat: IHeat;
  Adjudicator: IAdjudicator;
  Couple: ICouple;
  Mark: IMark;
};
type ActionIndex = 'reload' | 'create' | 'update' | 'delete' | 'start' | 'finish' | 'reset' | 'patch';
interface IGenericAction<M extends keyof ModelIndex, A extends ActionIndex> {
  action: A;
  model: M;
  data?: A extends 'create' | 'update' | 'delete' ? ModelIndex[M] : undefined;
  patch_data?: A extends 'patch' ? Partial<ModelIndex[M]> : undefined;
  href?: A extends 'reload' | 'start' | 'finish' | 'reset' ? string : undefined;
  dispatch?: (event: IAction) => void;
}

type IAction = IGenericAction<keyof ModelIndex, ActionIndex>;

const initialState: ITournamentState = {
  base_url: '',
  tournament: {
    id: 0,
    href: '',
    name: '',
    description: '',
    date: '',
    state: 'invalid',
    rounds_href: '',
    heats_href: '',
    adjudicators_href: '',
    couples_href: '',
    marks_href: '',
  },
  round_hrefs: [],
  rounds: {},
  heat_hrefs: [],
  heats: {},
  adjudicator_hrefs: [],
  adjudicators: {},
  couple_hrefs: [],
  couples: {},
  mark_hrefs: [],
  marks: {},
};

const crud_reducer = (state: ITournamentState, action: IAction) => {
  const model_name = `${action.model.toLowerCase()}s`; // hacky plural
  const model_hrefs = `${action.model.toLowerCase()}_hrefs`;
  const objects = state[model_name];
  const object_hrefs = state[model_hrefs];
  switch (action.action) {
    case 'reload': {
      const href = action.href || state.tournament[model_name + '_href'];
      fetch(state.base_url + href, { credentials: 'same-origin' })
        .then((response) => response.json())
        .then((data) => {
          data.results.map((item) => action.dispatch?.({ action: 'create', model: action.model, data: item }));
          if (data.next) action.dispatch?.({ action: 'reload', model: action.model, href: data.next });
        });
      return state;
    }
    case 'patch': {
      fetch(`${state.base_url}${action.href}`, {
        method: 'PATCH',
        body: JSON.stringify(action.patch_data),
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': get_cookie('csrftoken'),
        },
        credentials: 'same-origin',
      });
      return state;
    }
    case 'create': {
      const data = action.data as ModelIndex[keyof ModelIndex];
      return {
        ...state,
        [model_hrefs]: [...object_hrefs, data.href],
        [model_name]: { ...objects, [data.href]: data },
      };
    }
    case 'update': {
      const data = action.data as ModelIndex[keyof ModelIndex];
      return {
        ...state,
        [model_name]: { ...objects, [data.href]: data },
      };
    }
    case 'delete': {
      const data = action.data as ModelIndex[keyof ModelIndex];
      return {
        ...state,
        [model_hrefs]: object_hrefs.filter((item) => item != data.href),
        [model_name]: { ...objects, [data.href]: undefined },
      };
    }
  }
  console.log('Unknown event', action);
  return state;
};

const reducer: React.Reducer<ITournamentState, IAction> = (state: ITournamentState, action: IAction) => {
  if (action.model == 'Tournament') {
    switch (action.action) {
      case 'reload': {
        const href = action.href || state.tournament.href;
        fetch(state.base_url + href)
          .then((response) => response.json())
          .then((data) => action.dispatch?.({ action: 'update', model: 'Tournament', data: data }));
        return initialState;
      }
      case 'update': {
        const data = action.data as ITournament; // I would love the type system to know this now.
        if (data.id && state.tournament.id != data.id) {
          action.dispatch?.({ action: 'reload', model: 'Round' });
          action.dispatch?.({ action: 'reload', model: 'Heat' });
          action.dispatch?.({ action: 'reload', model: 'Adjudicator' });
          action.dispatch?.({ action: 'reload', model: 'Couple' });
          action.dispatch?.({ action: 'reload', model: 'Mark' });
          return { ...initialState, tournament: data };
        }
        return { ...state, tournament: data };
      }
      case 'delete':
        return initialState;
      case 'start':
      case 'finish':
      case 'reset': {
        fetch(`${state.base_url}${state.tournament.href}${action.action}/`, {
          method: 'POST',
          headers: {
            'X-CSRFToken': get_cookie('csrftoken'),
          },
          credentials: 'same-origin',
        });
        return state;
      }
    }
  }

  if (
    ['Round', 'Heat', 'Adjudicator', 'Couple', 'Mark'].indexOf(action.model) >= 0 &&
    ['reload', 'create', 'update', 'delete', 'patch'].indexOf(action.action) >= 0
  ) {
    return crud_reducer(state, action);
  }

  if (['Round', 'Heat'].indexOf(action.model) >= 0 && ['start', 'finish', 'reset'].indexOf(action.action) >= 0) {
    fetch(state.base_url + action.href + action.action + '/', { method: 'POST' });
    return state;
  }

  console.log('Unknown event', action);
  return state;
};

const TournamentContext = React.createContext<ITournamentContext>({
  tournamentState: initialState,
  dispatch: () => initialState,
});

const TournamentContextProvider: React.FunctionComponent<ITournamentContextProvider> = ({ id, children }) => {
  const [tournamentState, rawDispatch] = React.useReducer(reducer, initialState);

  const websocket = React.useRef<ReconWebSocket>();
  const dispatchMutex = React.useRef<number>(1);
  const dispatchQueue = React.useRef<Array<IAction>>([]);
  const { username } = useAuth();

  const dispatch = React.useCallback(
    function dispatch(action) {
      dispatchQueue.current.push(action);
      if (dispatchMutex.current == 1) {
        // These actions are "atomic" due to the nature of async
        dispatchMutex.current = 0;
        while (dispatchQueue.current.length > 0) {
          const queue = dispatchQueue.current;
          dispatchQueue.current = [];
          queue.map((action) => rawDispatch({ dispatch: dispatch, ...action }));
        }
        dispatchMutex.current = 1;
      }
    },
    [rawDispatch],
  );
  const reload_tournament = React.useCallback(() => {
    dispatch({
      action: 'reload',
      model: 'Tournament',
      href: `/api/v1/tournament/${id}/`,
    });
  }, [id, dispatch]);

  React.useEffect(() => {
    if (username) {
      const ws = new ReconWebSocket(`ws://${window.location.host}/ws/tournament/${id}/`);
      websocket.current = ws;
      websocket.current.onmessage = (e) => {
        const data = JSON.parse(e.data);
        dispatch(data);
      };
      reload_tournament();
    }
    return () => {
      websocket.current?.close();
      websocket.current = undefined;
    };
  }, [id, reload_tournament, dispatch, username]);

  return <TournamentContext.Provider value={{ tournamentState, dispatch }}>{children}</TournamentContext.Provider>;
};

export { ITournament, IRound, IHeat, IAdjudicator, ICouple, IMark, ITournamentState, IAction };
export { TournamentContext, TournamentContextProvider };
