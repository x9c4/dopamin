import * as React from 'react';
import { useSubmit, useLoaderData, useFetcher } from 'react-router-dom';
import {
  Button,
  PageSection,
  Pagination,
  Tab,
  Tabs,
  TabTitleText,
  Title,
  Toolbar,
  ToolbarContent,
  ToolbarItem,
  Spinner,
} from '@patternfly/react-core';
import { Table, Caption, Thead, Tr, Th, Tbody, Td } from '@patternfly/react-table';
import PlusIcon from '@patternfly/react-icons/dist/esm/icons/plus-icon';
import TrashIcon from '@patternfly/react-icons/dist/esm/icons/trash-icon';
import { ITournament, IAdjudicator, ICouple } from '@app/TournamentContext/TournamentContext';
import { InlineEdit, InlineUserSelect } from '@app/utils/InlineEdit';
import { NoFormTextInput, UserSelect } from '@app/utils/FormInput';

const AdjudicatorTable: React.FunctionComponent = () => {
  const fetcher = useFetcher();
  const postFetcher = useFetcher();
  const adjudicators = fetcher.data?.results as IAdjudicator[];
  const [page, setPage] = React.useState(1);
  const [perPage, setPerPage] = React.useState(20);

  const onSetPage = (_event: React.MouseEvent | React.KeyboardEvent | MouseEvent, newPage: number) => {
    console.log(newPage);
    setPage(newPage);
    fetcher.load(`adjudicator/?index&page=${newPage}&per_page=${perPage}`);
  };

  const onPerPageSelect = (
    _event: React.MouseEvent | React.KeyboardEvent | MouseEvent,
    newPerPage: number,
    newPage: number,
  ) => {
    setPerPage(newPerPage);
    setPage(newPage);
    fetcher.load(`adjudicator/?index&page=${newPage}&per_page=${newPerPage}`);
  };

  React.useEffect(() => {
    if (fetcher.state === 'idle' && !fetcher.data) {
      fetcher.load(`adjudicator/?index&page=${page}&per_page=${perPage}`);
    }
  }, [fetcher, page, perPage]);

  if (!fetcher.data) return <Spinner />;

  return (
    <Pagination
      itemCount={fetcher.data.count}
      page={page}
      perPage={perPage}
      onSetPage={onSetPage}
      onPerPageSelect={onPerPageSelect}
    >
      <postFetcher.Form id="new-adjudicator-form" method="post" action="adjudicator/?index" />
      <Table>
        <Caption>Adjudicators</Caption>
        <Thead>
          <Tr>
            <Th>Slug</Th>
            <Th>Name</Th>
            <Th>User</Th>
            <Th>Actions</Th>
          </Tr>
        </Thead>
        <Tbody>
          <Tr>
            <Td>
              <NoFormTextInput
                name="slug"
                form="new-adjudicator-form"
                error={postFetcher.data && postFetcher.data['slug']}
              />
            </Td>
            <Td>
              <NoFormTextInput
                name="name"
                form="new-adjudicator-form"
                error={postFetcher.data && postFetcher.data['name']}
              />
            </Td>
            <Td>
              <UserSelect id="new-adjudicator-form-user" name="user" form="new-adjudicator-form" />
            </Td>
            <Td>
              <Button form="new-adjudicator-form" type="submit">
                <PlusIcon />
              </Button>
            </Td>
          </Tr>
          {adjudicators?.map(({ id, slug, name, user, username }) => (
            <Tr key={id}>
              <Td>{slug}</Td>
              <Td>
                <InlineEdit attribute="name" action={`adjudicator/${id}`} value={name} />
              </Td>
              <Td>
                <InlineUserSelect
                  attribute="user"
                  action={`adjudicator/${id}`}
                  value={user ? { href: user, username } : null}
                />
              </Td>
              <Td>
                <Button
                  variant="danger"
                  onClick={() => postFetcher.submit(null, { method: 'DELETE', action: `adjudicator/${id}` })}
                >
                  <TrashIcon />
                </Button>
              </Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
    </Pagination>
  );
};

const CoupleTable: React.FunctionComponent = () => {
  const fetcher = useFetcher();
  const postFetcher = useFetcher();
  const couples = fetcher.data?.results as ICouple[];
  const [page, setPage] = React.useState(1);
  const [perPage, setPerPage] = React.useState(20);

  const onSetPage = (_event: React.MouseEvent | React.KeyboardEvent | MouseEvent, newPage: number) => {
    console.log(newPage);
    setPage(newPage);
    fetcher.load(`couple/?index&page=${newPage}&per_page=${perPage}`);
  };

  const onPerPageSelect = (
    _event: React.MouseEvent | React.KeyboardEvent | MouseEvent,
    newPerPage: number,
    newPage: number,
  ) => {
    setPerPage(newPerPage);
    setPage(newPage);
    fetcher.load(`couple/?index&page=${newPage}&per_page=${newPerPage}`);
  };

  React.useEffect(() => {
    if (fetcher.state === 'idle' && !fetcher.data) {
      fetcher.load(`couple/?index&page=${page}&per_page=${perPage}`);
    }
  }, [fetcher, page, perPage]);

  if (!fetcher.data) return <Spinner />;

  return (
    <Pagination
      itemCount={fetcher.data.count}
      page={page}
      perPage={perPage}
      onSetPage={onSetPage}
      onPerPageSelect={onPerPageSelect}
    >
      <postFetcher.Form id="new-couple-form" method="post" action="couple/?index" />
      <Table>
        <Caption>Couples</Caption>
        <Thead>
          <Tr>
            <Th>Number</Th>
            <Th>Lead</Th>
            <Th>Follow</Th>
            <Th>Actions</Th>
          </Tr>
        </Thead>
        <Tbody>
          <Tr>
            <Td>
              <NoFormTextInput
                name="number"
                form="new-couple-form"
                error={postFetcher.data && postFetcher.data['number']}
              />
            </Td>
            <Td>
              <NoFormTextInput
                name="lead"
                form="new-couple-form"
                error={postFetcher.data && postFetcher.data['lead']}
              />
            </Td>
            <Td>
              <NoFormTextInput
                name="follow"
                form="new-couple-form"
                error={postFetcher.data && postFetcher.data['follow']}
              />
            </Td>
            <Td>
              <Button form="new-couple-form" type="submit">
                <PlusIcon />
              </Button>
            </Td>
          </Tr>
          {couples?.map(({ id, number, lead, follow }) => (
            <Tr key={id}>
              <Td>{number}</Td>
              <Td>
                <InlineEdit attribute="lead" action={`couple/${id}`} value={lead} />
              </Td>
              <Td>
                <InlineEdit attribute="follow" action={`couple/${id}`} value={follow} />
              </Td>
              <Td>
                <Button
                  variant="danger"
                  onClick={() => postFetcher.submit(null, { method: 'DELETE', action: `couple/${id}` })}
                >
                  <TrashIcon />
                </Button>
              </Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
    </Pagination>
  );
};

export const TournamentPlanning: React.FunctionComponent = () => {
  const submit = useSubmit();
  const tournament = useLoaderData() as ITournament;
  const [activeTabKey, setActiveTabKey] = React.useState<string | number>(0);

  return (
    <>
      <PageSection>
        <Title headingLevel="h1" size="lg">
          {tournament.name} ({tournament.state})
        </Title>
      </PageSection>
      <PageSection>
        <Tabs activeKey={activeTabKey} onSelect={(_, tabKey) => setActiveTabKey(tabKey)} mountOnEnter>
          <Tab eventKey={0} title={<TabTitleText>Tournament</TabTitleText>}>
            <InlineEdit attribute="description" value={tournament.description || ''} />
            <Toolbar id="toolbar-items">
              <ToolbarContent>
                <ToolbarItem>
                  <Button variant="primary" onClick={() => submit(null, { method: 'POST', action: 'start' })}>
                    Start
                  </Button>
                </ToolbarItem>
                <ToolbarItem>
                  <Button variant="danger" onClick={() => submit(null, { method: 'DELETE' })}>
                    Delete
                  </Button>
                </ToolbarItem>
              </ToolbarContent>
            </Toolbar>
          </Tab>
          <Tab eventKey={1} title={<TabTitleText>Adjudicators</TabTitleText>}>
            <AdjudicatorTable />
          </Tab>
          <Tab eventKey={2} title={<TabTitleText>Couples</TabTitleText>}>
            <CoupleTable />
          </Tab>
        </Tabs>
      </PageSection>
    </>
  );
};
