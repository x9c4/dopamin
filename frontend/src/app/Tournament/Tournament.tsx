import * as React from 'react';
import { useNavigate, useLoaderData, useSubmit } from 'react-router-dom';
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Gallery,
  GalleryItem,
  PageSection,
  Title,
  Toolbar,
  ToolbarContent,
  ToolbarItem,
  CodeBlock,
  CodeBlockCode,
} from '@patternfly/react-core';
import { TournamentContext, TournamentContextProvider, ITournament } from '@app/TournamentContext/TournamentContext';
import { TournamentPlanning } from '@app/Tournament/TournamentPlanning';

const TournamentDashboard: React.FunctionComponent = () => {
  const navigate = useNavigate();
  const submit = useSubmit();
  const { tournamentState, dispatch } = React.useContext(TournamentContext);
  const { tournament } = tournamentState;

  const startTournament = () => {
    if (tournament) dispatch({ model: 'Tournament', action: 'start' });
  };

  const finishTournament = () => {
    if (tournament) dispatch({ model: 'Tournament', action: 'finish' });
  };

  const resetTournament = () => {
    if (tournament) dispatch({ model: 'Tournament', action: 'reset' });
  };

  return (
    <React.Fragment>
      <Title headingLevel="h1" size="lg">
        {tournament.name} ({tournament.state})
      </Title>
      <Toolbar id="toolbar-items">
        <ToolbarContent>
          <ToolbarItem>
            <Button variant="primary" isDisabled={tournament.state != 'planned'} onClick={startTournament}>
              Start
            </Button>
          </ToolbarItem>
          <ToolbarItem>
            <Button variant="primary" isDisabled={tournament.state != 'started'} onClick={finishTournament}>
              Finish
            </Button>
          </ToolbarItem>
          <ToolbarItem>
            <Button variant="danger" onClick={resetTournament}>
              Reset
            </Button>
          </ToolbarItem>
          <ToolbarItem>
            <Button variant="secondary" onClick={() => dispatch({ model: 'Tournament', action: 'reload' })}>
              Reload
            </Button>
          </ToolbarItem>
          <ToolbarItem>
            <Button variant="danger" onClick={() => submit(null, { method: 'DELETE' })}>
              Delete
            </Button>
          </ToolbarItem>
        </ToolbarContent>
      </Toolbar>
      <Gallery hasGutter>
        {Object.values(tournamentState.adjudicators).map((adjudicator) => (
          <GalleryItem key={adjudicator.href}>
            <Card
              onClick={() => {
                navigate('/tournaments/' + tournament.id + '/adjudicators/' + adjudicator.id + '/');
              }}
            >
              <CardTitle>{adjudicator.slug}</CardTitle>
              <CardBody>{adjudicator.name}</CardBody>
            </Card>
          </GalleryItem>
        ))}
      </Gallery>
      <CodeBlock>
        <CodeBlockCode>
          {JSON.stringify(tournamentState.tournament, null, 2)}
          <br />
          {JSON.stringify(tournamentState.rounds, null, 2)}
          <br />
          {JSON.stringify(tournamentState.heats, null, 2)}
          <br />
          {JSON.stringify(tournamentState.adjudicators, null, 2)}
          <br />
          {JSON.stringify(tournamentState.couples, null, 2)}
          <br />
          {JSON.stringify(tournamentState.marks, null, 2)}
          <br />
        </CodeBlockCode>
      </CodeBlock>
    </React.Fragment>
  );
};

const TournamentBoard: React.FunctionComponent = () => {
  const tournament = useLoaderData() as ITournament;

  if (tournament.state == 'planned') return <TournamentPlanning />;

  return (
    <TournamentContextProvider id={tournament.id}>
      <PageSection>
        <TournamentDashboard />
      </PageSection>
    </TournamentContextProvider>
  );
};

export { TournamentBoard };
