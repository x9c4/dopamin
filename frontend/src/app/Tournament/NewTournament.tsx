import * as React from 'react';
import { Form, useActionData } from 'react-router-dom';
import { Alert, Button, ButtonType, Form as PfForm, FormAlert, Modal, ModalVariant } from '@patternfly/react-core';
import { FormTextInput } from '@app/utils/FormInput';

export const NewTournamentModal: React.FunctionComponent = () => {
  const actionData = useActionData();
  const [isModalOpen, setModalOpen] = React.useState(false);

  const nonFieldErrors = actionData && actionData['non_field_errors'];

  const handleModalToggle = () => {
    setModalOpen(!isModalOpen);
  };
  return (
    <>
      <Button variant="primary" onClick={handleModalToggle}>
        New Tournament
      </Button>
      <Form noValidate id="new-tournament-form" method="post" />
      <Modal
        variant={ModalVariant.small}
        title="Create new tournament"
        isOpen={isModalOpen}
        onClose={handleModalToggle}
        actions={[
          <Button key="create" variant="primary" form="new-tournament-form" type={ButtonType.submit}>
            Create
          </Button>,
          <Button key="cancel" variant="link" onClick={handleModalToggle}>
            Cancel
          </Button>,
        ]}
      >
        <PfForm>
          {nonFieldErrors && (
            <FormAlert>
              <Alert variant="danger" title={nonFieldErrors.join('; ')} aria-live="polite" isInline />
            </FormAlert>
          )}
          <FormTextInput name="name" label="Name" isRequired form="new-tournament-form" />
          <FormTextInput name="description" label="Description" form="new-tournament-form" />
        </PfForm>
      </Modal>
    </>
  );
};
