import * as React from 'react';
import { useLoaderData } from 'react-router-dom';
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Gallery,
  GalleryItem,
  PageSection,
  Toolbar,
  ToolbarContent,
  ToolbarItem,
} from '@patternfly/react-core';
import { TournamentContext, TournamentContextProvider, ITournament } from '@app/TournamentContext/TournamentContext';
import { useDocumentTitle } from '@app/utils/useDocumentTitle';
import { NewTournamentModal } from '@app/Tournament/NewTournament';

interface ITournaments {
  count: number;
  results: ITournament[];
}

const TournamentCard: React.FunctionComponent = () => {
  const { tournamentState } = React.useContext(TournamentContext);
  const { tournament, adjudicator_hrefs, couple_hrefs, round_hrefs } = tournamentState;

  return (
    <Card id={`tournament-card-${tournament.id}`} isClickable>
      <CardHeader
        selectableActions={{
          selectableActionId: `tournament-card-action-${tournament.id}`,
          to: `/tournament/${tournament.id}/`,
        }}
      >
        <CardTitle>
          {tournament.name} ({tournament.state})
        </CardTitle>
      </CardHeader>
      <CardBody>
        <p>{tournament.description}</p>
        <p>Adjudicators: {adjudicator_hrefs.length}</p>
        <p>Couples: {couple_hrefs.length}</p>
        <p>Rounds: {round_hrefs.length}</p>
      </CardBody>
    </Card>
  );
};

const TournamentIndex: React.FunctionComponent = () => {
  useDocumentTitle('Dopamin | Tournaments');
  const tournaments = (useLoaderData() as ITournaments).results;

  const tournament_nodes: Array<React.ReactNode> = tournaments.map((tournament) => (
    <GalleryItem key={tournament.id.toString()}>
      <TournamentContextProvider id={tournament.id}>
        <TournamentCard />
      </TournamentContextProvider>
    </GalleryItem>
  ));

  return (
    <PageSection>
      <Toolbar>
        <ToolbarContent>
          <ToolbarItem>
            <NewTournamentModal />
          </ToolbarItem>
        </ToolbarContent>
      </Toolbar>
      <Gallery hasGutter>{tournament_nodes}</Gallery>
    </PageSection>
  );
};

export { TournamentIndex };
