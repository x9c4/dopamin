import * as React from 'react';
import '@patternfly/react-core/dist/styles/base.css';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { routes } from '@app/routes';
import '@app/app.css';

const router = createBrowserRouter(routes);

const App: React.FunctionComponent = () => <RouterProvider router={router} />;

export default App;
