import * as React from 'react';
import ReactDom from 'react-dom';
import { useParams } from 'react-router-dom';
import { css } from '@patternfly/react-styles';
import styles from '@patternfly/react-styles/css/utilities/Alignment/alignment';
import {
  Bullseye,
  Divider,
  ExpandableSection,
  ExpandableSectionToggle,
  Flex,
  FlexItem,
  PageSection,
  Split,
  SplitItem,
  Stack,
  StackItem,
  Title,
} from '@patternfly/react-core';
import { PendingIcon } from '@patternfly/react-icons';
import {
  TournamentContext,
  TournamentContextProvider,
  IRound,
  IHeat,
  IMark,
} from '@app/TournamentContext/TournamentContext';

type MarkProps = {
  mark: IMark;
  numCouples: number;
};

const Mark: React.FunctionComponent<MarkProps> = ({ mark }) => {
  const { tournamentState, dispatch } = React.useContext(TournamentContext);
  const { couples } = tournamentState;
  const couple = couples[mark.couple];

  const onMarkClick = () => {
    dispatch({
      action: 'patch',
      model: 'Mark',
      href: mark.href,
      patch_data: { value: mark.value == 1 ? 0 : 1 },
    });
  };
  const onRemarkClick = () => {
    dispatch({
      action: 'patch',
      model: 'Mark',
      href: mark.href,
      patch_data: { remark: (mark.remark + 1) % 4 },
    });
  };

  return (
    <svg width="50" height="100">
      <text textAnchor="middle" x="25" y="30" fontFamily="monospace" fontSize="24" fill="#000000">
        {couple.number}
      </text>
      {mark.remark >= 1 && <circle cx="10" cy="40" r="3" fill="#0000000" stroke="none" />}
      {mark.remark >= 2 && <circle cx="25" cy="40" r="3" fill="#0000000" stroke="none" />}
      {mark.remark >= 3 && <circle cx="40" cy="40" r="3" fill="#0000000" stroke="none" />}
      <rect x="1" y="1" width="48" height="48" fill="#00000000" onClick={onRemarkClick} />
      <g onClick={onMarkClick}>
        <rect x="2" y="52" width="46" height="46" fill="#ffffffff" stroke="#000000ff" strokeWidth="2" />
        {mark.value == 1 && (
          <path d="m 10,60 30,30 m -30,0 30,-30" fill="none" stroke="#000000ff" strokeWidth="4" strokeLinecap="round" />
        )}
      </g>
    </svg>
  );
};

const FinalMark: React.FunctionComponent<MarkProps> = ({ mark, numCouples }) => {
  const { tournamentState, dispatch } = React.useContext(TournamentContext);
  const { couples } = tournamentState;
  const couple = couples[mark.couple];

  const onMarkClick = (event) => {
    const index = event.currentTarget.dataset.index;
    dispatch({
      action: 'patch',
      model: 'Mark',
      href: mark.href,
      patch_data: { value: index == mark.value ? 0 : index },
    });
  };
  const onRemarkClick = () => {
    dispatch({
      action: 'patch',
      model: 'Mark',
      href: mark.href,
      patch_data: { remark: (mark.remark + 1) % 4 },
    });
  };
  const indices: Array<number> = [];
  for (let i = 1; i <= numCouples; i++) {
    indices.push(i);
  }

  return (
    <svg width="50" height={50 * (numCouples + 1)}>
      <text textAnchor="middle" x="25" y="30" fontFamily="monospace" fontSize="24" fill="#000000">
        {couple.number}
      </text>
      {mark.remark >= 1 && <circle cx="10" cy="40" r="3" fill="#0000000" stroke="none" />}
      {mark.remark >= 2 && <circle cx="25" cy="40" r="3" fill="#0000000" stroke="none" />}
      {mark.remark >= 3 && <circle cx="40" cy="40" r="3" fill="#0000000" stroke="none" />}
      <rect x="0" y="0" width="50" height="50" fill="#00000000" onClick={onRemarkClick} />
      {indices.map((i) => (
        <g key={i} onClick={onMarkClick} data-index={i}>
          <rect
            x="2"
            y={50 * i + 2}
            width="46"
            height="46"
            fill={mark.value == i ? '#00ff00ff' : '#ffffffff'}
            stroke="#000000ff"
            strokeWidth="2"
          />
          <text textAnchor="middle" x="25" y={50 * i + 30} fontFamily="monospace" fontSize="20" fill="#000000">
            {i}
          </text>
        </g>
      ))}
    </svg>
  );
};

type HeatPanelProps = {
  adjudicator_href: string;
  round: IRound;
  heat: IHeat;
};

const HeatPanel: React.FunctionComponent<HeatPanelProps> = ({ adjudicator_href, round, heat }) => {
  const { tournamentState } = React.useContext(TournamentContext);
  const { mark_hrefs, marks } = tournamentState;

  const ThisMark = round.round_type == 'final' ? FinalMark : Mark;

  return (
    <React.Fragment>
      <Divider />
      <Split>
        <SplitItem>
          <Title headingLevel="h3">Heat {heat.number}</Title>
        </SplitItem>
        <SplitItem isFilled></SplitItem>
        <SplitItem>
          {
            mark_hrefs
              .map((mark_href) => marks[mark_href])
              .filter((mark) => mark.adjudicator == adjudicator_href && mark.heat == heat.href && mark.value != 0)
              .length
          }
        </SplitItem>
      </Split>
      <Flex>
        {mark_hrefs
          .map((mark_href) => marks[mark_href])
          .filter((mark) => mark.adjudicator == adjudicator_href && mark.heat == heat.href)
          .map((mark) => (
            <FlexItem key={mark.href}>
              <ThisMark mark={mark} numCouples={heat.couples.length} />
            </FlexItem>
          ))}
      </Flex>
    </React.Fragment>
  );
};

type RoundSheetProps = {
  round_href: string;
  adjudicator_href: string;
};

const RoundSheet: React.FunctionComponent<RoundSheetProps> = ({ round_href, adjudicator_href }) => {
  const [expandedDances, setExpandedDances] = React.useState<Array<string>>([]);
  const { tournamentState } = React.useContext(TournamentContext);
  const { rounds, heats, mark_hrefs, marks } = tournamentState;

  const toggleDance = (dance) => {
    const index = expandedDances.indexOf(dance);
    setExpandedDances(
      index >= 0
        ? [...expandedDances.slice(0, index), ...expandedDances.slice(index + 1, expandedDances.length)]
        : [...expandedDances, dance],
    );
  };
  const round = rounds[round_href];

  const danceMarks = Object.assign(
    {},
    ...round.dances.map((dance) => ({
      [dance]: mark_hrefs
        .map((mark_href) => marks[mark_href])
        .filter((mark) => mark.adjudicator == adjudicator_href && heats[mark.heat]?.dance == dance && mark.value != 0)
        .length,
    })),
  );
  const headerPortalDiv = document.getElementById('header-portal');
  const headerPortalChildren =
    Object.values(danceMarks).join('; ') + (round.marks_target ? ' / ' + round.marks_target : '');

  return (
    <Stack>
      {ReactDom.createPortal(headerPortalChildren, headerPortalDiv)}
      {round.dances.map((dance) => (
        <StackItem key={dance}>
          <Split>
            <SplitItem>
              <ExpandableSectionToggle
                contentId={dance}
                isExpanded={expandedDances.includes(dance)}
                onToggle={() => toggleDance(dance)}
              >
                <Title headingLevel="h2">{dance}</Title>
              </ExpandableSectionToggle>
            </SplitItem>
            <SplitItem isFilled></SplitItem>
            <SplitItem>
              {danceMarks[dance]}
              {round.marks_target ? ' / ' + round.marks_target : ''}
            </SplitItem>
          </Split>
          <ExpandableSection contentId={dance} isExpanded={expandedDances.includes(dance)} isDetached>
            {round.heats
              .map((heat_href) => heats[heat_href])
              .filter((heat) => heat?.dance == dance)
              .sort((first, second) => (first.number > second.number ? 1 : -1))
              .map((heat) => (
                <HeatPanel key={heat.href} adjudicator_href={adjudicator_href} round={round} heat={heat} />
              ))}
          </ExpandableSection>
        </StackItem>
      ))}
    </Stack>
  );
};

type MarkSheetProps = {
  adjudicator_href: string;
};

const MarkSheet: React.FunctionComponent<MarkSheetProps> = ({ adjudicator_href }) => {
  const [currentRound, setCurrentRound] = React.useState('');
  const { tournamentState } = React.useContext(TournamentContext);
  const { tournament, adjudicators, round_hrefs, rounds } = tournamentState;
  const adjudicator = adjudicators[adjudicator_href];

  React.useEffect(() => {
    if (rounds[currentRound]?.state != 'started') {
      const round_href = round_hrefs.find((round_href) => rounds[round_href].state == 'started') || '';
      setCurrentRound(round_href);
    }
  }, [currentRound, round_hrefs, rounds]);

  if (adjudicator === undefined) return <React.Fragment />;

  return (
    <React.Fragment>
      <Title headingLevel="h2">
        <Split>
          <SplitItem>
            {adjudicator.name} ({adjudicator.slug})
          </SplitItem>
          <SplitItem isFilled className={css(styles.textAlignCenter)}>
            {currentRound && rounds[currentRound].slug}
          </SplitItem>
          <SplitItem>{tournament.name}</SplitItem>
        </Split>
      </Title>
      {currentRound ? (
        <RoundSheet round_href={currentRound} adjudicator_href={adjudicator.href} />
      ) : (
        <Bullseye>
          <PendingIcon size="xl" />
        </Bullseye>
      )}
    </React.Fragment>
  );
};

type AdjudicatorBoardParams = {
  tournament_id: string;
  adjudicator_id: string;
};

const AdjudicatorBoard: React.FunctionComponent = () => {
  const { tournament_id, adjudicator_id } = useParams<AdjudicatorBoardParams>();
  if (!tournament_id || !adjudicator_id) {
    return null;
  }

  return (
    <PageSection>
      <TournamentContextProvider id={+tournament_id}>
        <MarkSheet adjudicator_href={'/api/v1/adjudicators/' + +adjudicator_id + '/'} />
      </TournamentContextProvider>
    </PageSection>
  );
};

export { AdjudicatorBoard, MarkSheet };
