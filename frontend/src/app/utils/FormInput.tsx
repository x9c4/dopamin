import * as React from 'react';
import { useActionData, useFetcher } from 'react-router-dom';
import {
  Button,
  Checkbox,
  FormGroup,
  FormHelperText,
  HelperText,
  HelperTextItem,
  MenuToggle,
  MenuToggleElement,
  Select,
  SelectList,
  SelectOption,
  TextInput,
  TextInputGroup,
  TextInputGroupMain,
  TextInputGroupUtilities,
} from '@patternfly/react-core';
import HourglassIcon from '@patternfly/react-icons/dist/esm/icons/hourglass-icon';
import TimesIcon from '@patternfly/react-icons/dist/esm/icons/times-icon';

export type InputType =
  | 'number'
  | 'search'
  | 'time'
  | 'text'
  | 'date'
  | 'datetime-local'
  | 'email'
  | 'month'
  | 'password'
  | 'tel'
  | 'url';

type NoFormTextInputProps = {
  name: string;
  label?: string;
  form: string;
  type?: InputType;
  isRequired?: boolean;
  error?: string;
  initValue?: string;
};

export const NoFormTextInput: React.FunctionComponent<NoFormTextInputProps> = ({
  name,
  label,
  form,
  type = 'text',
  isRequired,
  error,
  initValue = '',
}) => {
  const [value, setValue] = React.useState(initValue);
  const fieldId = `${form}-${name}`;

  const handleChange = (_, value) => {
    setValue(value);
  };

  React.useEffect(() => {
    const form_element = document.getElementById(form);
    const listener = () => setValue(initValue);
    form_element?.addEventListener('reset', listener);
    return () => {
      form_element?.removeEventListener('reset', listener);
    };
  }, [form, initValue]);

  return (
    <FormGroup label={label} isRequired={isRequired} fieldId={fieldId}>
      <TextInput
        id={fieldId}
        validated={error ? 'error' : 'default'}
        isRequired={isRequired}
        type={type}
        form={form}
        name={name}
        value={value}
        onChange={handleChange}
      />
      {error && (
        <FormHelperText>
          <HelperText>
            <HelperTextItem hasIcon variant="error">
              {error}
            </HelperTextItem>
          </HelperText>
        </FormHelperText>
      )}
    </FormGroup>
  );
};

type FormTextInputProps = {
  name: string;
  label?: string;
  form: string;
  type?: InputType;
  isRequired?: boolean;
};
export const FormTextInput: React.FunctionComponent<FormTextInputProps> = ({
  name,
  label,
  form,
  type = 'text',
  isRequired,
}) => {
  const actionData = useActionData();
  const error = actionData && actionData[name];

  return <NoFormTextInput name={name} label={label} form={form} type={type} isRequired={isRequired} error={error} />;
};

type NoFormCheckboxInputProps = {
  name: string;
  label: string;
  form: string;
  isRequired?: boolean;
  error?: string;
};
export const NoFormCheckboxInput: React.FunctionComponent<NoFormCheckboxInputProps> = ({
  name,
  label,
  form,
  isRequired,
  error,
}) => {
  const [isChecked, setIsChecked] = React.useState(false);
  const fieldId = `${form}-${name}`;

  const handleChange = (_, value) => {
    setIsChecked(value);
  };

  return (
    <FormGroup label={label} isRequired={isRequired} fieldId={fieldId}>
      <Checkbox
        isRequired={isRequired}
        form={form}
        id={fieldId}
        name={name}
        isChecked={isChecked}
        onChange={handleChange}
      />
      {error && (
        <FormHelperText>
          <HelperText>
            <HelperTextItem hasIcon variant="error">
              {error}
            </HelperTextItem>
          </HelperText>
        </FormHelperText>
      )}
    </FormGroup>
  );
};

type FormCheckboxInputProps = {
  name: string;
  label: string;
  form: string;
  isRequired?: boolean;
};
export const FormCheckboxInput: React.FunctionComponent<FormCheckboxInputProps> = ({
  name,
  label,
  form,
  isRequired,
}) => {
  const actionData = useActionData();
  const error = actionData && actionData[name];

  return <NoFormCheckboxInput name={name} label={label} form={form} isRequired={isRequired} error={error} />;
};

export interface IUser {
  username?: string;
  href: string;
}
type UserSelectProps = {
  id?: string;
  name: string;
  form: string;
  error?: string;
  initValue?: null | IUser;
};
export const UserSelect: React.FunctionComponent<UserSelectProps> = ({ name, form, error, initValue = null }) => {
  const userFetcher = useFetcher();
  const [isOpen, setIsOpen] = React.useState(false);
  const [selected, setSelected] = React.useState<null | IUser>(initValue);
  const [inputValue, setInputValue] = React.useState<string>(initValue?.username || '');
  const [focusedItemIndex, setFocusedItemIndex] = React.useState<number | null>(null);
  const textInputRef = React.useRef<HTMLInputElement>();
  const timeout = React.useRef<null | ReturnType<typeof setTimeout>>(null);
  const fieldId = `${form}-${name}`;
  const userList: IUser[] = userFetcher.data?.results || [];

  const onTextInputChange = (_event: React.FormEvent<HTMLInputElement>, value: string) => {
    if (inputValue === value) return;
    setInputValue(value);
    if (value == '') {
      setSelected(null);
    } else {
      if (value.length >= 2) {
        setSelected(null);
        timeout.current && clearTimeout(timeout.current);
        timeout.current = setTimeout(() => userFetcher.load(`/user/?index&username__icontains=${value}`), 500);
      }
    }
  };

  const onToggleClick = () => setIsOpen(!isOpen);

  const onSelect = (_event: React.MouseEvent<Element, MouseEvent> | undefined, value: string | number | undefined) => {
    const user = userList.find((user) => user.href == value);
    if (user) {
      setInputValue(user.username || '');
      setSelected(user);
    }
    setIsOpen(false);
    setFocusedItemIndex(null);
  };

  const handleMenuArrowKeys = (key: string) => {
    let indexToFocus;

    if (isOpen) {
      if (key === 'ArrowUp') {
        // When no index is set or at the first index, focus to the last, otherwise decrement focus index
        if (focusedItemIndex === null || focusedItemIndex === 0) {
          indexToFocus = userList.length - 1;
        } else {
          indexToFocus = focusedItemIndex - 1;
        }
      }

      if (key === 'ArrowDown') {
        // When no index is set or at the last index, focus to the first, otherwise increment focus index
        if (focusedItemIndex === null || focusedItemIndex === userList.length - 1) {
          indexToFocus = 0;
        } else {
          indexToFocus = focusedItemIndex + 1;
        }
      }

      setFocusedItemIndex(indexToFocus);
    }
  };

  const onInputKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const enabledMenuItems = userList;
    const [firstMenuItem] = enabledMenuItems;
    const focusedItem = focusedItemIndex ? enabledMenuItems[focusedItemIndex] : firstMenuItem;

    switch (event.key) {
      // Select the first available option
      case 'Enter':
        if (isOpen) {
          setInputValue(focusedItem.username || '');
          setSelected(focusedItem);
        }

        setIsOpen((prevIsOpen) => !prevIsOpen);
        setFocusedItemIndex(null);

        break;
      case 'Tab':
      case 'Escape':
        setIsOpen(false);
        break;
      case 'ArrowUp':
      case 'ArrowDown':
        event.preventDefault();
        handleMenuArrowKeys(event.key);
        break;
    }
  };

  React.useEffect(() => {
    const form_element = document.getElementById(form);
    const listener = () => {
      setIsOpen(false);
      setSelected(initValue);
      setInputValue(initValue?.username || '');
    };
    form_element?.addEventListener('reset', listener);
    return () => {
      form_element?.removeEventListener('reset', listener);
    };
  }, [form, initValue]);

  const toggle = (toggleRef: React.Ref<MenuToggleElement>) => (
    <MenuToggle
      ref={toggleRef}
      variant="typeahead"
      onClick={onToggleClick}
      isExpanded={isOpen}
      isFullWidth
      icon={userFetcher.state != 'idle' && <HourglassIcon />}
    >
      <TextInputGroup isPlain>
        <TextInputGroupMain
          value={inputValue}
          onClick={onToggleClick}
          onChange={onTextInputChange}
          onKeyDown={onInputKeyDown}
          id={`${fieldId}-typeahead-input`}
          autoComplete="off"
          innerRef={textInputRef}
          placeholder="Start typing..."
          role="combobox"
          isExpanded={isOpen}
        />

        <TextInputGroupUtilities>
          {!!inputValue && (
            <Button
              variant="plain"
              onClick={() => {
                setSelected(null);
                setInputValue('');
                textInputRef?.current?.focus();
              }}
            >
              <TimesIcon />
            </Button>
          )}
        </TextInputGroupUtilities>
      </TextInputGroup>
    </MenuToggle>
  );

  return (
    <>
      <TextInput id={fieldId} name={name} form={form} value={selected?.href || ''} readOnly hidden />
      <Select
        id={`${fieldId}`}
        isOpen={isOpen}
        selected={selected?.username}
        onSelect={onSelect}
        onOpenChange={() => setIsOpen(false)}
        toggle={toggle}
      >
        <SelectList id={`${fieldId}-listbox`}>
          {userList?.map((user, index) => {
            const { href, username } = user;
            const id = href.split('/')[4];
            return (
              <SelectOption
                id={`${fieldId}-listbox-${id}`}
                key={id}
                value={href}
                onClick={() => setSelected(user)}
                isFocused={focusedItemIndex === index}
                ref={null}
              >
                {username}
              </SelectOption>
            );
          })}
        </SelectList>
      </Select>
      {error && (
        <FormHelperText>
          <HelperText>
            <HelperTextItem hasIcon variant="error">
              {error}
            </HelperTextItem>
          </HelperText>
        </FormHelperText>
      )}
    </>
  );
};
