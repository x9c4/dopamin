class ReconWebSocket {
  private ws: null | WebSocket;
  private url: string;
  private timeout: number;
  private timeoutInterval;
  onopen;
  onmessage;

  constructor(url: string) {
    this.ws = null;
    this.url = url;
    this.timeout = 250;
    this.connect();
  }

  private connect(): void {
    this.ws = new WebSocket(this.url);
    this.ws.onopen = () => {
      console.log('Websocket connected ' + this.url);
      this.timeout = 250;
      clearTimeout(this.timeoutInterval);
      this.onopen && this.onopen();
    };
    this.ws.onclose = (event) => {
      this.timeout = Math.min(2 * this.timeout, 10000); // increase timeout
      console.log('Websocket is closed. Reconnect in', this.timeout / 1000, ' seconds.', event.reason);
      this.timeoutInterval = setTimeout(() => this.connect(), this.timeout);
    };
    this.ws.onerror = (event) => {
      console.log('Error: ' + event);
    };
    this.ws.onmessage = (event) => {
      this.onmessage && this.onmessage(event);
    };
  }

  close(): void {
    if (this.ws) {
      this.ws.onclose = () => {
        return null;
      };
      this.ws.close();
    }
  }

  send(text: string): void {
    if (this.ws) {
      this.ws.send(text);
    }
  }
}

export { ReconWebSocket };
