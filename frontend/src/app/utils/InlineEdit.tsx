import * as React from 'react';
import { useFetcher } from 'react-router-dom';
import { Button, ButtonType, FormHelperText, HelperText, HelperTextItem } from '@patternfly/react-core';
import CheckIcon from '@patternfly/react-icons/dist/esm/icons/check-icon';
import TimesIcon from '@patternfly/react-icons/dist/esm/icons/times-icon';
import HourglassIcon from '@patternfly/react-icons/dist/esm/icons/hourglass-icon';
import PencilAltIcon from '@patternfly/react-icons/dist/esm/icons/pencil-alt-icon';
import styles from '@patternfly/react-styles/css/components/InlineEdit/inline-edit';
import { css } from '@patternfly/react-styles';
import { InputType, NoFormTextInput, UserSelect, IUser } from '@app/utils/FormInput';

interface IValidationErrors {
  [Key: string]: string[];
}

interface InlineEditProps {
  type?: InputType;
  attribute: string;
  action?: string;
  value: string;
  altvalue?: string;
}
export const InlineEdit: React.FunctionComponent<InlineEditProps> = ({ type, attribute, action, value, altvalue }) => {
  const [editable, setEditable] = React.useState(false);
  const [validationErrors, setValidationErrors] = React.useState<IValidationErrors>({});
  const formRef = React.useRef<null | HTMLFormElement>(null);
  const fetcher = useFetcher();
  const actionData = fetcher.data as IValidationErrors | null;
  const field_errors = validationErrors[attribute]?.join(';');
  const non_field_errors = validationErrors?.non_field_errors?.join(';');
  const action_stub = action ? action.replace('/', '_') + '-' : '';
  const formId = `inline-edit-${action_stub}${attribute}`;

  React.useEffect(() => {
    if (actionData) {
      setValidationErrors(actionData);
    } else {
      formRef.current?.reset();
    }
  }, [value, actionData, fetcher.state]);

  return (
    <fetcher.Form
      className={css(styles.inlineEdit, editable && styles.modifiers.inlineEditable)}
      method="patch"
      action={action}
      id={formId}
      ref={formRef}
      onReset={() => setEditable(false)}
    >
      <div className={css(styles.inlineEditGroup)}>
        <div className={css(styles.inlineEditValue)}>{value || altvalue}</div>
        <div className={css(styles.inlineEditAction, styles.modifiers.enableEditable)}>
          <Button
            variant="plain"
            onClick={() => {
              setValidationErrors({});
              setEditable(true);
            }}
          >
            <PencilAltIcon />
          </Button>
        </div>
      </div>
      <div className={css(styles.inlineEditGroup)}>
        <div className={css(styles.inlineEditInput)}>
          <NoFormTextInput
            error={field_errors}
            isRequired
            type={type}
            name={attribute}
            initValue={value}
            form={formId}
          />
          {non_field_errors && (
            <FormHelperText>
              <HelperText>
                <HelperTextItem hasIcon variant="error">
                  {non_field_errors}
                </HelperTextItem>
              </HelperText>
            </FormHelperText>
          )}
        </div>
        <div className={css(styles.inlineEditGroup, styles.modifiers.actionGroup, styles.modifiers.iconGroup)}>
          <div className={css(styles.inlineEditAction, styles.modifiers.valid)}>
            <Button variant="plain" type={ButtonType.submit} isActive={fetcher.state !== 'idle'}>
              {fetcher.state === 'idle' ? <CheckIcon /> : <HourglassIcon />}
            </Button>
          </div>
          <div className={css(styles.inlineEditAction, styles.modifiers.plain)}>
            <Button variant="plain" type={ButtonType.reset} isActive={fetcher.state !== 'idle'}>
              <TimesIcon />
            </Button>
          </div>
        </div>
      </div>
    </fetcher.Form>
  );
};

interface InlineUserSelectProps {
  attribute: string;
  action?: string;
  value: null | IUser;
  altvalue?: string;
}
export const InlineUserSelect: React.FunctionComponent<InlineUserSelectProps> = ({
  attribute,
  action,
  value,
  altvalue,
}) => {
  const [editable, setEditable] = React.useState(false);
  const [validationErrors, setValidationErrors] = React.useState<IValidationErrors>({});
  const formRef = React.useRef<null | HTMLFormElement>(null);
  const fetcher = useFetcher();
  const actionData = fetcher.data as IValidationErrors | null;
  const field_errors = validationErrors[attribute]?.join(';');
  const non_field_errors = validationErrors?.non_field_errors?.join(';');
  const action_stub = action ? action.replace('/', '_') + '-' : '';
  const formId = `inline-edit-${action_stub}${attribute}`;

  React.useEffect(() => {
    if (actionData) {
      setValidationErrors(actionData);
    } else {
      formRef.current?.reset();
    }
  }, [value, actionData, fetcher.state]);

  return (
    <fetcher.Form
      className={css(styles.inlineEdit, editable && styles.modifiers.inlineEditable)}
      method="patch"
      action={action}
      id={formId}
      ref={formRef}
      onReset={() => setEditable(false)}
    >
      <div className={css(styles.inlineEditGroup)}>
        <div className={css(styles.inlineEditValue)}>{value?.username || altvalue}</div>
        <div className={css(styles.inlineEditAction, styles.modifiers.enableEditable)}>
          <Button
            variant="plain"
            onClick={() => {
              setValidationErrors({});
              setEditable(true);
            }}
          >
            <PencilAltIcon />
          </Button>
        </div>
      </div>
      <div className={css(styles.inlineEditGroup)}>
        <div className={css(styles.inlineEditInput)}>
          <UserSelect error={field_errors} name={attribute} initValue={value} form={formId} />
          {non_field_errors && (
            <FormHelperText>
              <HelperText>
                <HelperTextItem hasIcon variant="error">
                  {non_field_errors}
                </HelperTextItem>
              </HelperText>
            </FormHelperText>
          )}
        </div>
        <div className={css(styles.inlineEditGroup, styles.modifiers.actionGroup, styles.modifiers.iconGroup)}>
          <div className={css(styles.inlineEditAction, styles.modifiers.valid)}>
            <Button variant="plain" type={ButtonType.submit} isActive={fetcher.state !== 'idle'}>
              {fetcher.state === 'idle' ? <CheckIcon /> : <HourglassIcon />}
            </Button>
          </div>
          <div className={css(styles.inlineEditAction, styles.modifiers.plain)}>
            <Button variant="plain" type={ButtonType.reset} isActive={fetcher.state !== 'idle'}>
              <TimesIcon />
            </Button>
          </div>
        </div>
      </div>
    </fetcher.Form>
  );
};
