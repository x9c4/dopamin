import * as React from 'react';
import { Alert, AlertGroup, AlertActionCloseButton, AlertVariant } from '@patternfly/react-core';
import { ReconWebSocket } from '@app/utils/ReconWebSocket';

interface IAlert {
  key: number;
  title: string;
  variant: string;
  message: null | string;
  timeout: number;
}

const NotificationContext: React.FunctionComponent = () => {
  const [alerts, setAlerts] = React.useState<Array<IAlert>>([]);
  const websocket = React.useRef<ReconWebSocket>();
  const serial = React.useRef<number>(0);

  const removeAlert = (key) => {
    setAlerts([...alerts.filter((el) => el.key != key)]);
  };

  React.useEffect(() => {
    if (!websocket.current) {
      const ws = new ReconWebSocket(`ws://${window.location.host}/ws/notification/`);
      websocket.current = ws;
      ws.onopen = () => {
        ws.send(JSON.stringify({ type: 'notification', title: 'Hello World!' }));
      };
      ws.onmessage = (event) => {
        const data = JSON.parse(event.data);
        if (data.type == 'notification') {
          const alert: IAlert = {
            key: serial.current++,
            title: data.title,
            variant: data.variant || 'default',
            message: data?.message,
            timeout: data.timeout || 0,
          };
          setAlerts((s) => [...s, alert]);
        }
      };
    }
    return () => {
      websocket.current?.close();
      websocket.current = undefined;
    };
  }, []);

  return (
    <AlertGroup isToast isLiveRegion>
      {alerts.map(({ key, title, variant, message, timeout }) => (
        <Alert
          variant={AlertVariant[variant]}
          title={title}
          timeout={timeout}
          key={key}
          actionClose={
            <AlertActionCloseButton title={title} variantLabel={`${variant} alert`} onClose={() => removeAlert(key)} />
          }
        >
          {message}
        </Alert>
      ))}
    </AlertGroup>
  );
};

export { NotificationContext };
