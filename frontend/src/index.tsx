import React from 'react';
import ReactDOM from 'react-dom/client';
import App from '@app/index';

if (process.env.NODE_ENV !== 'production') {
  const config = {
    rules: [
      {
        id: 'color-contrast',
        enabled: false,
      },
    ],
  };
  // eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
  const axe = require('@axe-core/react');
  axe(React, ReactDOM, 1000, config);
}

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(<App />);
