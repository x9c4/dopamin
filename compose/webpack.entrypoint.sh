#!/bin/sh

yarn install

if [ "${DOPAMIN_DEV}" ]
then
  yarn build:dev
else
  yarn build
fi
