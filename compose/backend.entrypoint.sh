#!/bin/sh

if [ "${DOPAMIN_DEV}" ]
then
  pip3 install --no-cache-dir -e . django-extensions ipython ipdb httpie jq -r test-requirements.txt
fi
exec python3 "$@"
