black:
	isort .
	black .
	cd frontend ; yarnpkg format

lint:
	black --diff --check .
	isort -c --diff .
	flake8
	# mypy
	cd frontend ; yarnpkg lint
	@echo "🙊 Code 🙈 LGTM 🙉 !"
