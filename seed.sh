#!/bin/bash

set -eux

./manage.py flush --noinput

BASE=":8000"

T1_HREF="$(http "${BASE}/api/v1/tournament/" name=Lara description=Kirschkernweitspucken | jq -r '.href')"

for i in A B C
do
  http "${BASE}${T1_HREF}adjudicator/" slug="${i}"
done

for i in $(seq 1 4)
do
  COUPLES[i]="$(http "${BASE}${T1_HREF}couple/" number="${i}" | jq -r '.href')"
done

R1_HREF="$(http "${BASE}${T1_HREF}round/" slug="Vorrunde_1" dances:='["Polka", "Handjive"]' | jq -r '.href')"

http "${BASE}${R1_HREF}qualify/" post=1
http "${BASE}${R1_HREF}shuffle/" num_heats=2 heats:='[{"dance":"Polka", "number":1, "couples":["'"${COUPLES[1]}"'", "'"${COUPLES[2]}"'"]}, {"dance":"Polka", "number":2, "couples":["'"${COUPLES[3]}"'", "'"${COUPLES[4]}"'"]}, {"dance":"Handjive", "number":1, "couples":["'"${COUPLES[1]}"'", "'"${COUPLES[3]}"'"]}, {"dance":"Handjive", "number":2, "couples":["'"${COUPLES[2]}"'", "'"${COUPLES[4]}"'"]}]'

R1_HREF="$(http "${BASE}${T1_HREF}round/" slug="Halbfinale" dances:='["Polka", "Handjive"]' | jq -r '.href')"
http "${BASE}${R1_HREF}qualify/" post=1
http "${BASE}${R1_HREF}shuffle/" num_heats=2

T2_HREF="$(http "${BASE}${T1_HREF}tournament/" name=Max description=Straussenrennen | jq -r '.href')"

for i in D E F
do
  http "${BASE}${T2_HREF}adjudicator/" slug="${i}" name="${i}${i,} ${i}${i,}"
done

for i in $(seq 1 31)
do
  http "${BASE}${T2_HREF}couple/" number="${i}"
done

R2_HREF="$(http "${BASE}${T2_HREF}round/" slug="Vorrunde_1" dances:='["Polka", "Handjive"]' marks_target=16 | jq -r '.href')"
http "${BASE}${R2_HREF}qualify/" post=1
http "${BASE}${R2_HREF}shuffle/" num_heats=2

http "${BASE}${T2_HREF}start/" post=1
http "${BASE}${R2_HREF}start/" post=1
