"""dopamin URL Configuration """

from django.urls import include, path
from drf_spectacular.views import SpectacularAPIView

from backend.urls import urlpatterns as v1_urlpatterns
from backend.urls import v0_router

from .views import favicon, index, not_found

urlpatterns = [
    path("api/v1/", SpectacularAPIView.as_view(), name="schema"),
    path("api/v1/", include(v1_urlpatterns)),
    path("api/v0/", include(v0_router.urls)),
    path("api/<path:route>", not_found),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path("images/favicon.png", favicon, name="favicon"),
    path("", index, name="index"),
    path("<path:route>", index),
]
