from django.shortcuts import Http404, redirect, render


async def index(request, route=None):
    return render(request, "index.html")


async def favicon(request):
    return redirect("/static/images/favicon.png")


async def not_found(request, route=None):
    raise Http404()
