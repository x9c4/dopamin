from django.urls import reverse


def test_index(client):
    response = client.get(reverse("index"))
    assert response.status_code == 200


def test_schema(client):
    response = client.get(reverse("schema"))
    assert response.status_code == 200


def test_favicon(client):
    response = client.get(reverse("favicon"))
    assert response.status_code == 302
    assert response.url == "/static/images/favicon.png"


def test_not_found(client):
    response = client.get("/api/v1/foo/")
    assert response.status_code == 404
